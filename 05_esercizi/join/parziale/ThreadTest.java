import java.util.*;

public class ThreadTest {

public static void main(String[] args) {
	boolean order;
    if(args.length != 2)
    	System.out.println("USO: java ThreadTest numThreads");
 	else{
		System.out.println(args[1]);
		if(args[1].equals(true))
			order = true;
		else
			order =false;
		ThreadTest test = new ThreadTest();
		test.threadTest(Integer.parseInt(args[0]),order);
 		System.out.println("");
    }
}

private void threadTest(int numOfThreads,boolean order) {
	Thread[] threads = new Thread[numOfThreads];

	for (int i = 0; i < threads.length; i++) {
		threads[i] = new MyThread(i);
    	threads[i].start();
    	if(order==true)
    	{
			try{
				threads[i].join();
				//System.out.println("terminato thread numero " +i); 
			}
			catch(InterruptedException ie	)
			{}
		}
	}

	if(order==false)
	{
		for (int i = 0; i < threads.length; i++)
		{
			
			try{
				threads[i].join();
				//System.out.println("terminato thread numero " +i);
			}
			catch(InterruptedException ie	)
			{}
			
		}
	}
	
	try{
		Thread.sleep((int)(Math.random() * 1200));
 	} 
	catch(InterruptedException e) {}
	System.out.println("\nTutti i thread figli rientrati?"); 
}

class MyThread extends Thread {
    int id;

	public MyThread(int i){
		id = i;
	}

 	public void run() {
		try{
			Thread.sleep((int)(Math.random() * 1000));
     	} 
		catch(InterruptedException e) {}
		System.out.print(id+" tornato.\n");
	}

}

}
