class ReadersWriters{  
	public static void main(String[] args)
	{
		if(args.length < 2)
			System.out.println("USO: java ReadersWriters numReaders numWriters s");
		else
		{
			int numReaders = Integer.parseInt(args[0]);
			int numWriters = Integer.parseInt(args[1]);

			ReadWriteCoord rw = new ReadWriteCoord();
			for (int i = 0; i < numReaders; i++)
				new Reader(i, rw);
			for (int i = 0; i < numWriters; i++)
				new Writer(numReaders + i, rw);
		}
	}
}

class Reader implements Runnable{  
	private int id;
   	private ReadWriteCoord rw;

   	public Reader(int i, ReadWriteCoord c){  
		id = i;
      	rw = c;
      	new Thread(this).start();
   	}

   	public void run(){  
		for(int i=0; i < 10; i++){  
			try{
				Thread.sleep((int)(Math.random() * 100));
            }  
			catch(InterruptedException e) {}
         	rw.startRead(id);
         	// lettura
         	try{
				Thread.sleep((int)(Math.random() * 100));
           	}  
			catch(InterruptedException e) {}
         	rw.endRead(id);
      	}
   	}
}

class Writer implements Runnable{  
	private int id;
   	private ReadWriteCoord rw;

   	public Writer(int i, ReadWriteCoord c){  
		id = i;
      	rw = c;
      	new Thread(this).start();
   	}

   	public void run(){  
		for(int i=0; i < 10; i++){  
			try{
				Thread.sleep((int)(Math.random() * 100));
            }  
			catch(InterruptedException e) {}
         	rw.startWrite(id);
         	// scrittura
         	try{
				Thread.sleep((int)(Math.random() * 100));
            }  
			catch(InterruptedException e) {}
         	rw.endWrite(id);
      	}
   	}
}

class ReadWriteCoord{  
	private int numReaders = 0;
   	private int numWriters = 0;

   	public synchronized void startRead(int i)
   	{
			while(numWriters!=0)
			{
				try
				{
					wait();
				}
				catch(InterruptedException ie)
				{}
			}
				numReaders++;
				System.out.println("Start reading " + i + "[readers: "+numReaders+", writers: "+numWriters+"]");
   	}

   	public synchronized void endRead(int i)
   	{
			numReaders--; 
      	System.out.println("End reading " + i + "[readers: "+numReaders+", writers: "+numWriters+"]");
   	}

   	public synchronized void startWrite(int i)
   	{
			while(numReaders!=0 && numWriters!=0)
			{
				try
				{
					wait();
				}
				catch(InterruptedException ie)
				{}
			}
				numWriters++;
				System.out.println("Start writing " + i + "[readers: "+numReaders+", writers: "+numWriters+"]");
			
   	}

   	public synchronized void endWrite(int i)
   	{
			notifyAll();
			numWriters--;
			System.out.println("End writing " + i + "[readers: "+numReaders+", writers: "+numWriters+"]");
   	}

}
