import java.util.concurrent.locks.*;

// Implementa i lettori-scrittori usando il
// ReadWriteLock di Java 5.0

class ReadersWritersLock{ 
	
	public static void main(String[] args){ 
	    if(args.length < 2)
			System.out.println("USO: java ReadersWritersLock numReaders numWriters"); 
		else{
			int numReaders = Integer.parseInt(args[0]);
      		int numWriters = Integer.parseInt(args[1]);

      		Database db = new Database();
      		for (int i = 0; i < numReaders; i++)
        		new Reader(i, db);
      		for (int i = 0; i < numWriters; i++)
        		new Writer(numReaders + i, db); 
		}
   	}
}


class Database{  
	private ReadWriteLock rwl = new ReentrantReadWriteLock();
   	private Lock rl = rwl.readLock();
   	private Lock wl = rwl.writeLock();
	private int numReaders = 0;
	private int numWriters = 0;

   	public void read(int i)
   	{
			rl.lock();
			numReaders++;
      	System.out.println("Start reading " + i + "[readers: "+numReaders+", writers: "+numWriters+"]");
      	try{
			Thread.sleep((int)(Math.random() * 100));
			} 
			catch(InterruptedException e) {}
			numReaders--;
      	System.out.println("End reading " + i + "[readers: "+numReaders+", writers: "+numWriters+"]");
      	rl.unlock();
   	}

   	public void write(int i)
   	{
			wl.lock();
			numWriters++;
			System.out.println("Start writing " + i + "[readers: "+numReaders+", writers: "+numWriters+"]");
			try{
			Thread.sleep((int)(Math.random() * 100));
			} 	
			catch(InterruptedException e) {}
			numWriters--;
			System.out.println("End writing " + i + "[readers: "+numReaders+", writers: "+numWriters+"]");
			wl.unlock();
		}

}


class Reader implements Runnable{  
	private int id;
   	private Database db;

  	public Reader(int i, Database d){  
		id = i;
      	db = d;
      	new Thread(this).start();
   	}

   	public void run(){  
		for(int i=0; i < 10; i++){  
			try{
				Thread.sleep((int)(Math.random() * 100));
            } 
			catch(InterruptedException e) {}
         	db.read(id);
         	try{
				Thread.sleep((int)(Math.random() * 100));
            }
			catch(InterruptedException e) {}
      	}
   	}
}



class Writer implements Runnable{
	private int id;
   	private Database db;

   	public Writer(int i, Database d){  
		id = i;
      	db = d;
      	new Thread(this).start();
   	}

   	public void run(){  
		for(int i=0; i < 10; i++){  
			try{
				Thread.sleep((int)(Math.random() * 100));
            } 
			catch(InterruptedException e) {}
            db.write(id);
         	try{
				Thread.sleep((int)(Math.random() * 100));
            }  
			catch(InterruptedException e) {}
      	}
   	}
}
