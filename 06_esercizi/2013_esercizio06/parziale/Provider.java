import java.net.*;
import java.io.*;
import java.util.*;

public class Provider {

	private int port = 7777;
	private ServerSocket s = null;
	
	public void activate() throws IOException {
		try { 
			s = new ServerSocket(port);
		} catch(IOException e) {
			System.err.println("could not listen on port "+port);
			System.exit(1);
		}
		System.out.println("Server listening on port "+port);
		while(true){ //server cicla per sempre
			Socket s1 = s.accept(); //stabilita connessione, crea thread e avvialo
			ServerThread st = new ServerThread(s1);
			st.start();
		}
	}
	
	public static void main(String[] a) throws IOException{
		Provider s = new Provider();
		s.activate();
	}
}

class ServerThread extends Thread { //ogni thread servirà un client diverso
	
	Socket connection = null; //passato da thread principale come parametro
	ObjectOutputStream out;
	ObjectInputStream in;
	int[] myArray; //array per i/o dati con client
	String clientName;
	
	public ServerThread(Socket socket){
		super("ServerThread");
		connection = socket;
	}

	public void run(){
		try{
			in = new ObjectInputStream(connection.getInputStream());  
			out = new ObjectOutputStream(connection.getOutputStream());
			out.flush();
			try{ 
				clientName = (String)in.readObject(); //capire CHI mi sta parlando
			}
			catch(ClassNotFoundException classnot){
				System.err.println("Data received in unknown format");
			}
			System.out.println("\nConnected with "+clientName);
			boolean exit = false; //condizione di uscita da loop
			do{ //questo è ciclo: cliente può chiedere servizio (sort) o dirmi 'ok, ho finito' (bye)
				try{
					String auxMsg = (String)in.readObject(); //possibili: "bye" o "sort"
					if(auxMsg.equals("bye")){ //client non vuole più comunicare 
						System.out.println("\nClosing connection with "+clientName);
						exit = true; //uscirò dal while
						sendMessage("bye",false);
					}
					else{ //devo ordinare un array   				
						myArray = (int[])in.readObject();  //leggo array da ordinare
						System.out.println("\nclient ["+clientName+"]> " + printArray(myArray));
						Arrays.sort(myArray);
						Thread.sleep((int)(Math.random() * 500)); //simulo 'tempo computazionale' per ordinare; facilita interleaving stampe.
						sendMessage(myArray,true); 
					}
				}
				catch(ClassNotFoundException classnot){
					System.err.println("Data received in unknown format");
				}
			}while(!exit);
		}
		catch(InterruptedException e) {
			System.out.println("Sleep interrotta!");
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
		finally{
			//sia in caso che di comportamento nominale che di eccezione, uscendo chiudo tutto.
			try{
			   	in.close();
				out.close();
				connection.close();
			}
			catch(IOException ioException){
				ioException.printStackTrace();
			}
		}
	}

	public void sendMessage(Object msg, boolean intPrint){ //nota: passo stringa o array come msg.
		try{
			out.writeObject(msg);
			out.flush();
			if(intPrint)
				System.out.println("server> ["+clientName+"]'s sorted array is: " + printArray((int [])msg));
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}	

	String printArray(int [] arr){ 
		String res = "";
		for (int i=0; i<arr.length; i++)
			res+=(arr[i]+" ");
		return res;
	}
}  