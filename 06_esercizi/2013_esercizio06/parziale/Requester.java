import java.io.*;
import java.net.*; 
import java.util.*;

public class Requester{
	Socket requestSocket;
	ObjectOutputStream out;
 	ObjectInputStream in;
 	int[] message;
	String name;
	private int port = 7777;
	Socket socket;
	
	public Requester(String aName){
		name=aName;
	}
	
	
	public void go(){
		try
		{
			//1: creare socket per connettermi con il server
			socket = new Socket("localhost",port);
			//2: inizializzare streams
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			sendMessage(name); //NB! comunico il nome
			//3: comunicare con il server
				//3.1: attesa (per simulare tempo di decisione)
				Thread.sleep((long) (Math.random()*3000));
				//3.2: decidere se mandare 'sort' o 'bye'
				int tmp=(int)(Math.random()*2);
				System.out.println("Il numero casuale per la scelta è: "+tmp);
				if (tmp==1)
				{
					System.out.println("messagio bye");
					sendMessage("bye");
				}
				//3.3: se 'sort', generare array, mandarlo, leggere array ordinato
				else
				{
					System.out.println("messagio sort");
					sendMessage("sort");
					int [] temp = createArray();
					System.out.println(printArray(temp));
					sendMessage(temp);
					System.out.println("\n messagio inviato");
					message = (int[])in.readObject();
					System.out.println("array risposta ricevuto");
					System.out.println(printArray(message));
					System.out.println("\n"+" array printato");
					System.out.println("messagio bye");
					sendMessage("bye");
				}
		//4: catturare eccezioni e chiudere tutto
		}
		catch(UnknownHostException uhe)
		{}
		catch(IOException ioe)
		{}
		catch(InterruptedException ie)
		{}
		catch(ClassNotFoundException cnfe)
		{}
		finally
		{
			try
			{
				out.close();
				in.close();
				socket.close();
			}
			catch(IOException ioe)
			{}
		}
	}
	
	void sendMessage(Object msg){ 
		try{
			out.writeObject(msg);
			out.flush();
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	
	int[] createArray(){ //crea array interi con dimensione ed elementi random
		Random r = new Random();
		int[] ris = new int[r.nextInt(19)+2];
		for (int i=0; i<ris.length; i++) {
			ris[i] = r.nextInt(100);
		}
		return ris;
	}
		
	
	String printArray(int [] arr){  //stampo array el per el
		String res = "";
		for (int i=0; i<arr.length; i++)
			res+=(arr[i]+" ");
		return res;
	}

	public static void main(String args[]){
		Requester client = new Requester(args[0]);
		client.go();
	} 

}
