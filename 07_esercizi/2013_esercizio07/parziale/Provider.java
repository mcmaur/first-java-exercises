//[3]: Inserire gli import necessari
                      
//[4]: completare la dichiarazione della classe con exends.. e/o implements..
public class Provider {


 
	public static void main(String[] args) {
    	try {
//[5]: chiamare il costruttore, bind
	   	} 
	     	catch (Exception e) {
	       		System.out.println("Server Error: " + e);
	     	}
    }
	
//[6]: la signature di alcuni metodi va estesa con throws..
	public Provider(){
    	super();
	}

	public int[] mySort(String cname, int[] arr){
		System.out.println("\n"+cname+" <<< "+printArray(arr));
		try {Thread.sleep((int)(Math.random() * 2000));} catch(InterruptedException e){}
		Arrays.sort(arr);
		System.out.println("\n"+cname+" >>> "+printArray(arr));
		return arr;
	}

	static String printArray(int [] arr){  //stampo array el per el
		String res = "";
		for (int i=0; i<arr.length; i++)
			res+=(arr[i]+" ");
		return res;
	}
}