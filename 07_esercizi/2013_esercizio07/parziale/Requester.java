//[7]: inserire gli import necessari  

public class Requester {

    public static void main(String[] args) {
        
	try {
//[8] Creare collegamento all'oggetto remoto tramite rmiregistry			

	    // Ora si usa c per chiamare metodi remoti
	    for (int i=0; i<10; i++) { 
			int[] arr = createArray();
			System.out.println("\nmando "+printArray(arr));
//[9] Inserire chiamata di metodo remoto
			System.out.println("ricevo "+printArray(arr));
	    }
    }

//[10] Catturare Eccezioni

}

static int[] createArray(){ //crea array interi con dimensione ed elementi random
	Random r = new Random();
	int[] ris = new int[r.nextInt(19)+2];
 	for (int i=0; i<ris.length; i++) {
		ris[i] = r.nextInt(100);
	}
	return ris;
} 
	   	
static String printArray(int [] arr){  //stampo array el per el
	String res = "";
	for (int i=0; i<arr.length; i++)
		res+=(arr[i]+" ");
	return res;
}

}