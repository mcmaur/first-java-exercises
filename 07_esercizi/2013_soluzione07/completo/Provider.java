import java.rmi.*;
import java.lang.*; 
import java.util.*;

public class Provider extends java.rmi.server.UnicastRemoteObject implements SortService {

    public Provider()
        throws RemoteException {
        super();
    }
 
	public static void main(String[] args) {
    	try {
	       		SortService c = new Provider();
	       		Naming.bind("rmi://localhost/SomeSortService", c);
	   	} 
	     	catch (Exception e) {
	       		System.out.println("Server Error: " + e);
	     	}
    }
	

	public int[] mySort(String cname, int[] arr) throws java.rmi.RemoteException {
		System.out.println("\n"+cname+" <<< "+printArray(arr));
		try {Thread.sleep((int)(Math.random() * 2000));} catch(InterruptedException e){}
		Arrays.sort(arr);
		System.out.println("\n"+cname+" >>> "+printArray(arr));
		return arr;
	}

	static String printArray(int [] arr){  //stampo array el per el
		String res = "";
		for (int i=0; i<arr.length; i++)
			res+=(arr[i]+" ");
		return res;
	}
}