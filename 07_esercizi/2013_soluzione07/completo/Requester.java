import java.rmi.Naming;
import java.rmi.RemoteException;
import java.net.MalformedURLException;	
import java.rmi.NotBoundException; 
import java.util.*;   

public class Requester {

    public static void main(String[] args) {
        
	try {
	    // Crea collegamento all'oggetto remoto tramite rmiregistry			
            SortService c = (SortService) Naming.lookup("rmi://localhost/SomeSortService");
	    // Ora si usa c per chiamare metodi remoti
	    for (int i=0; i<10; i++) { 
			int[] arr = createArray();
			System.out.println("\nmando "+printArray(arr));
       		arr = c.mySort(args[0],arr);
			System.out.println("ricevo "+printArray(arr));
	    }
    }

    // Eccezioni
	catch (MalformedURLException murle) {
            System.out.println("MalformedURLException"+murle);
    }
    catch (RemoteException re) {
        System.out.println("RemoteException"+re);
    }
    catch (NotBoundException nbe) {
        System.out.println("NotBoundException"+nbe);
    }
}

static int[] createArray(){ //crea array interi con dimensione ed elementi random
	Random r = new Random();
	int[] ris = new int[r.nextInt(19)+2];
 	for (int i=0; i<ris.length; i++) {
		ris[i] = r.nextInt(100);
	}
	return ris;
} 
	
static int[] createConstant(int k){ //crea array interi con dimensione ed elementi random
	int[] ris = new int[5];
	for (int i=0; i<ris.length; i++) {
		ris[i] = k;
	}
	return ris;
}
		
	
static String printArray(int [] arr){  //stampo array el per el
	String res = "";
	for (int i=0; i<arr.length; i++)
		res+=(arr[i]+" ");
	return res;
}

}