import java.rmi.*;
import java.rmi.server.*;
import javax.naming.*;

class Client
{
   public static void main(String[] args)
   {
      try
      {
        Context namingContext = new InitialContext();
        Server server = (Server)namingContext.lookup("rmi:server");

        Employee emp1, emp2;
      	emp1 = new EmployeeImpl("Verdi", 35000);
      	emp2 = new EmployeeImpl("Rossi", 75000);
      	Pair p = new PairImpl(emp1, emp2);

        System.out.println(p.getFirst());
        System.out.println(p.getSecond());  
		server.sendPair(p);      
		try{Thread.sleep(1000);}catch(Exception e){}
        System.out.println("------------------------"); 
		System.out.println(p.getFirst());
        System.out.println(p.getSecond());
      }
      catch(Exception e)
      {
         e.printStackTrace();
      }
   }
}