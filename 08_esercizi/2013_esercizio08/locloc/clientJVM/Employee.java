import java.rmi.*;

public interface Employee
{
  	String getName();
   double getSalary();
   void raiseSalary(double byPercent);
   String toString();
}
