import java.io.Serializable;

class EmployeeImpl implements Employee, Serializable
{  public EmployeeImpl(String n, double s)
   {  name = n;
      salary = s;
   }

   public String getName()
   {  return name;
   }

   public double getSalary()
   {  return salary;
   }

   public void raiseSalary(double byPercent)
   {  double raise = salary * byPercent / 100;
      salary += raise;
   }
   
   public String toString() {
   	return name + "   " + salary;
   }

   private String name;
   private double salary;
}
