
import java.rmi.*;

public interface Server extends Remote
{
   void sendPair(Pair p) throws RemoteException;
}
