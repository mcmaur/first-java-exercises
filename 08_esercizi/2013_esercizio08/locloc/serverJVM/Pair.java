import java.rmi.*;

public interface Pair
{
  	void sort();
   Employee getFirst();
	Employee getSecond();
}
