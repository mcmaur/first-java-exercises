import java.io.Serializable;

class PairImpl implements Pair, Serializable
{  private Employee emp1,emp2;

public PairImpl (Employee e1, Employee e2)
   {  emp1 = e1;
      emp2 = e2;
   }

   public void sort()
   {  if (emp2.getName().compareTo(emp1.getName()) < 0) 
   	{Employee temp = emp1;
   	emp1 = emp2;
   	emp2 = temp;}
   }

   public Employee getFirst()
   {  return emp1;
   }

	public Employee getSecond()
   {  return emp2;
   }

}
