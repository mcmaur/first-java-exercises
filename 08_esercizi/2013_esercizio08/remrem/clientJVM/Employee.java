import java.rmi.*;

public interface Employee  extends Remote
{
  	String getName() throws RemoteException;
   double getSalary() throws RemoteException;
   void raiseSalary(double byPercent) throws RemoteException;
   String getString() throws RemoteException;
}
