import java.rmi.*;
import java.rmi.server.*;
import javax.naming.*;

class EmployeeImpl extends UnicastRemoteObject implements Employee
{  public EmployeeImpl(String n, double s)   throws RemoteException
   {  name = n;
      salary = s;
   }

   public String getName()
   {  return name;
   }

   public double getSalary()
   {  return salary;
   }

   public void raiseSalary(double byPercent)
   {  double raise = salary * byPercent / 100;
      salary += raise;
   }
   
   public String getString() {
   	return name + "   " + salary;
   }

   private String name;
   private double salary;
}
