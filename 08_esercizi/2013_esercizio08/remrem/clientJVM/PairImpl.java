import java.rmi.*;
import java.rmi.server.*;
import javax.naming.*;

class PairImpl extends UnicastRemoteObject
                     implements Pair

{  private Employee emp1,emp2;

public PairImpl (Employee e1, Employee e2) throws RemoteException
   {  super();
   	emp1 = e1;
      emp2 = e2;
   }

   public void sort()  throws RemoteException
   {  if (emp2.getName().compareTo(emp1.getName()) < 0) 
   	{Employee temp = emp1;
   	emp1 = emp2;
   	emp2 = temp;}
//System.out.println("DONE SORT");
   }

   public Employee getFirst()
   {  
//	System.out.println("DONE GET ONE");
	return emp1;
   }

	public Employee getSecond()
   {  
//	System.out.println("DONE GET TWO");
	return emp2;
   }

}
