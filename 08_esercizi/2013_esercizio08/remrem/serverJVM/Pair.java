import java.rmi.*;

public interface Pair extends Remote
{
  	void sort() throws RemoteException;
   Employee getFirst() throws RemoteException;
	Employee getSecond() throws RemoteException;
}
