import java.rmi.*;
import java.rmi.server.*;
import javax.naming.*;

class ServerImpl extends UnicastRemoteObject
                     implements Server
{
   public ServerImpl() throws RemoteException
   {
      super();
   }

   public void sendPair(Pair p)
   {
      try
      {
         p.sort();
         p.getFirst().raiseSalary(10);
         p.getSecond().raiseSalary(10);

         System.out.println(p.getFirst().getString());
         System.out.println(p.getSecond().getString());
      }
      catch(Exception e)
      {
         e.printStackTrace();
      }
   }

   public static void main(String[] args)
   {
      try
      {
         ServerImpl s = new ServerImpl();
         Context namingContext = new InitialContext();
         namingContext.bind("rmi:server", s);
         System.out.println("bound in registry");
      }
      catch(Exception e)
      {
         e.printStackTrace();
      }
   }
}