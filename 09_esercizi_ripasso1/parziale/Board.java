import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class Board implements ActionListener, Observer {
	private JFrame window = new JFrame();
	private JButton buttons[] = new JButton[9];
	private int count = 0;
	String myToken; //client gioca con X, server con O
	String oppToken;
	boolean isServer;
	Server myServer;
	Client myClient;

//overload costruttore: uno per client, uno per server
public Board(Client aClient){
	isServer = false;
	myClient = aClient;
	initialize();
}

public Board(Server aServer){
	isServer = true;
	myServer = aServer;
	initialize();
}

public void initialize(){
	window.setSize(300,300);
	if(isServer){
		window.setLocation(100,450);
		window.setTitle("wait for opponent..");
    	myToken = "O";
		oppToken = "X";
	}
	else {
		window.setLocation(700,450);
		window.setTitle("YOUR TURN!");
    	myToken = "X";
		oppToken = "O";
	}
	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


	/*
	TO DO: [8 righe]
	- settare un layout manager (e per il tris la scelta è abbastanza obbligata)
	- inserire i 9 bottoni (usate array buttons[] definito sopra)
	- per ogni bottone aggiungere actionListener
	- più avanti c'è un getActionCommand()..
	- volendo, rendere bottoni non Focusable
	*/
	window.setLayout(new GridLayout(3,3));
	for(int i=0;i<buttons.length;i++)
	{
		buttons[i]= new JButton();
		window.add(buttons[i]);
		buttons[i].addActionListener(this);
		buttons[i].setActionCommand(""+i);
		buttons[i].setFocusable(false);
	}

	if(isServer) //comincia a giocare il client, quindi il server parte con board disattivata
		deactivate();

	window.setVisible(true);
}


public void actionPerformed(ActionEvent a) {
	count++;//numero di mosse del giocatore

	JButton pressedButton = (JButton)a.getSource(); 
	pressedButton.setText(myToken); //mossa vera e propria su mia board
	pressedButton.setEnabled(false);

	String state = "match"; //stato per capire se ho vinto o pareggiato
	if(wins(myToken))
		state="win";
	else if(isDraw())
		state="draw";

	boolean isLast = !state.equals("match");
	Message messaggio = new Message(pressedButton.getActionCommand(),myToken,isLast);//bottone,simbolo,èultimo?
	//prima salvo messaggio in var di mio client/server, poi faccio notify
	if(isServer){
		myServer.msg=messaggio;
		synchronized(myServer){myServer.notify();}
	}
	else{
		myClient.msg=messaggio;
		synchronized(myClient){myClient.notify();}	
	}
	if(state.equals("win")) //ho vinto
		display("YOU WIN");
	else if(state.equals("draw")) //ho pareggiato
		display("DRAW");
	else{ //mossa comune (nb: dopo MIA mossa non posso aver perso)
 		window.setTitle("wait for opponent..");
		deactivate();
	}

}

public boolean wins(String player){ //controlla se dato simbolo è in pattern vincente
	boolean[] marks = new boolean[9];
	for (int i=0;i<9;i++)
		marks[i]=buttons[i].getText().equals(player);
	if(marks[0]&&marks[1]&&marks[2]) return true;
	if(marks[3]&&marks[4]&&marks[5]) return true;
	if(marks[6]&&marks[7]&&marks[8]) return true;//orizzontali
	if(marks[0]&&marks[3]&&marks[6]) return true;
	if(marks[1]&&marks[4]&&marks[7]) return true;
	if(marks[2]&&marks[5]&&marks[8]) return true;//verticali
	if(marks[0]&&marks[4]&&marks[8]) return true;
	if(marks[2]&&marks[4]&&marks[6]) return true;//diagonali
	return false;
}

public void update(Observable o, Object arg){//il mio client/server ha ricevuto un messaggio!
	Message m=(Message)arg;
	buttons[Integer.parseInt(m.pos)].setText(m.sign);
	buttons[Integer.parseInt(m.pos)].setEnabled(false);
	if(wins(oppToken))
		display("YOU LOSE");
	else if(m.last)
		display("DRAW");
	else{	
		window.setTitle("YOUR TURN!");
		reactivate();//mio turno, tasti tornano cliccabili
	}
}

public void display(String message){ //messaggio di fine partita e chiusura
	JOptionPane.showMessageDialog(window, message);
	System.exit(0);
}

public void deactivate(){//rendo board non cliccabile
	for (int i=0;i<9 ;i++ )
		buttons[i].setEnabled(false);
}

public void reactivate(){//rendo caselle vuote cliccabili
	for (int i=0;i<9 ;i++ ){
		if(buttons[i].getText().equals(""))
			buttons[i].setEnabled(true);
	}
}

public boolean isDraw(){//siccome parte il client, ultima mossa sarà del client (la 5°)
	return (!isServer && (count==5));
}

}
