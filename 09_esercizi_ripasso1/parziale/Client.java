import java.io.*;
import java.net.*; 
import java.util.*;

public class Client extends Observable implements Runnable{

	Socket requestSocket;
	ObjectOutputStream out;
 	ObjectInputStream in;
	String name;
	int port = 7777;
	Message msg;
	
	public Client(String s){
		name=s;
		msg = null;
	}
	
	public synchronized void run(){
		try{

			/*
			TO DO: [9+11 righe]
			- creare connessione con il server
			- inizializzazione streams
			- [se volete], scambio di stringhe per confermare connessione
			+
			loop del gioco vero e proprio. Il client deve:
			- aspettare (letteralmente) una mossa dal giocatore (evento in Board)
			- mandare messaggio (actionPerformed in Board modifica mia variabile msg)
			- leggere messaggio in arrivo su ObjectInputStream
			- avvertire la board (che lo osserva)
			- sia per messaggi in arrivo da board che da stream, se mossa è ultima (Message.last) uscire dal loop
			*/

			Socket socket = new Socket("localhost",port);
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
			while(true)
			{
				wait();
System.out.println("1"+msg);
				sendMessage(msg);
System.out.println("2"+msg);
				msg = (Message) in.readObject();
System.out.println("3"+msg);
				setChanged();
				notifyObservers(msg);				

				if(msg.last)
					break;
			}

			}
		catch(Exception e){e.printStackTrace();}
	}
		
	void sendMessage(Object msg){ //String XOR Message
		try{
			out.writeObject(msg);
			out.flush();
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}	

	
}
