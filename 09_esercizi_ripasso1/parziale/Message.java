import java.io.*;

//Message è oggetto passato via socket fra client e server
public class Message implements Serializable{
	String pos;
	String sign;
	boolean last;
	
	public Message(String pos, String sign, boolean last){
		this.pos=pos; //esempio: 5a cella
		this.sign=sign; //'X' XOR 'O'
		this.last=last; //è ultima mossa?
	}
	
	public String toString(){
		return " move: "+pos+" -> "+sign+" (last: "+last+")";
	}
}