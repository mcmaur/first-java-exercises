import java.net.*;
import java.io.*;
import java.util.*;

public class Server extends Observable implements Runnable{

	int port = 7777;
	ServerSocket s;
	ObjectOutputStream out;
	ObjectInputStream in;
	String clientName;
	Message msg;
	
	public synchronized void run(){
		try{

			/*
			TO DO: [9+11 righe]
			- creare connessione con il client
			- inizializzazione streams
			- [se volete], scambio di stringhe per confermare connessione
			+
			loop del gioco vero e proprio. Il server deve:  //NOTA: ordine NON è lo stesso!
			- leggere messaggio in arrivo su ObjectInputStream
			- avvertire la board (che lo osserva)						
			- aspettare (letteralmente) una mossa dal giocatore (evento in Board)
			- mandare messaggio (actionPerformed in Board modifica mia variabile msg)
			- sia per messaggi in arrivo da board che da stream, se mossa è ultima (Message.last) uscire dal loop
			*/
			ServerSocket serversocket = new ServerSocket(port);
			Socket socket = serversocket.accept();
			ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			while(true)
			{
				msg = (Message) in.readObject();
				
				setChanged();
				notifyObservers(msg);
				
				wait();

				sendMessage(msg);

				if(msg.last)
					break;
			}
		}
		catch(Exception e){e.printStackTrace();}
	}
	
	void sendMessage(Object msg){ 
		try{
			out.writeObject(msg);
			out.flush();
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}	
}
