public class Tris
{

	public static void main(String[] args) throws Exception
	{
		/*
		TO DO: [16 righe]
		controllare se primo argomento è 'client' o 'server', poi
		- creare client/server
		- creare board
		- board osserva client/server
		- creare thread ed avviarlo
		- se param sono sbagliati, stampare messaggio di avvertimento/istruzioni
		*/
		Board board;
		Server server;
		Client client;
		Thread t;
		
		if(args.length<1)
		{
			System.out.println("Not right number of argument");
			throw new IllegalArgumentException();
		}
		if(args[0].equalsIgnoreCase("client"))
		{
			if(args.length!=2)
			{
				System.out.println("Please insert also the client name");
				throw new IllegalArgumentException();
			}
				
			client = new Client(args[1]);
			board = new Board(client);
			client.addObserver(board);
			t = new Thread(client);
			t.start();
		}
		else
		{
			if(args.length!=1)
			{
				System.out.println("Too much arguments written");
				throw new IllegalArgumentException();
			}
			server = new Server();
			board = new Board(server);
			server.addObserver(board);
			t = new Thread(server);
			t.start();
		}

	}

}
