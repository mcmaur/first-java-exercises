/*
public static boolean eOrdinato(String[] a)

Riceve un array di stringhe come parametro.
Restituisce true se l'array è ordinato lessicograficamente (cioe', se le stringhe dell'array compaiono in ordine lessicografico crescente), false altrimenti.

Es: [anna, fabio, giorgio] e' ordinato lessicograficamente;
    [anna, giorgio, fabio] NON e' ordinato lessicograficamente.

public static boolean lunghezzaStringhe(String[] s, int a, int b)

Prende come parametro un array di stringhe e due numeri interi a e b.
Restituisce true se l'array contiene almeno una stringa di lunghezza compresa tra a e b (si assuma che a<b).

NB: anche in questo caso, il ciclo di verifica sull'array si puo' fermare appena trova la prima stringa la cui lunghezza e' compresa tra a e b./*Esercizi su Array di String:

Aggiungere alla classe ArrayEsercizi2.java il seguente metodo statico.

// ***************************************************
public static String[] spostaInFondo(String[] vett, String s)

Riceve come parametro un array di stringhe (non ordinato) e una stringa s.
Modifica l'array spostando al fondo tutte le occorrenze di s nell'array (NB: un'occorrenza di s nell'array corrisponde al fatto che una cella dell'array ha valore s).
Restituisce l'array modificato.

Es: dato [Gigi, Luca, Giorgio, Luca, Marino] e s=Luca si ottiene {Gigi, Marino, Giorgio, Luca, Luca]

NB: Il numero di operazioni dell'algoritmo deve essere proporzionale alla lunghezza dell'array.
Suggerimento: si usino due indici: uno per scorrere l'array da sinistra a destra e l'altro per scorrerlo da destra a sinistra. Per spostare un'occorrenza di s in fondo all'array, la si scambi con l'ultima stringa dell'array diversa da s.*/

public class ArrayEsercizi2
{
	public static boolean eOrdinato(String[] a)
	{
		boolean trovato= true;
		for(int i=0;i<a.length-1 && trovato == true;i++)
		{
			if(a[i].compareTo(a[i+1])>0)
				trovato= false;
		}
		return trovato;
		
	}

	public static boolean lunghezzaStringhe(String[] s, int a, int b)
	{
		boolean trovato=false;
		for (int i=0;i<s.length && trovato==false;i++)
		{
			if (s [i].length()>=a && s [i].length()<=b)
			trovato=true;
		}
		return trovato;
	}
	public static String[] spostaInFondo(String[] vett, String s)
	{
		String z="";
		int i;
		int j = vett.length-1;
		while(j>=0 && vett[j].compareTo(s) == 0)
			j--;

		for(i=0;i<j;i++)
		{
			if(s.compareTo(vett[i])==0)
			{
				z=vett[j];
				vett[j]=vett[i];
				vett[i]=z;
				j--;
			}
		}
		return vett;
	}
}
