/* La classe ArrayUtil e' una libreria per la gestione di array di
numeri e (dopo opportuna estensione) di array di stringhe.
Offre i metodi per la creazione ed inizializzazione di tali tipi di array, e per
visualizzare a video il loro contenuto.
Tutti i metodi di ArrayUtil sono statici.
Molto utile in un'applicazione Java per gestire array
senza riscrivere ogni volta l'intero codice che serve a creare, inizializzare array
e a visualizzarne a video il contenuto.
*/

import java.util.*;
public class ArrayUtil
{


/* Il metodo statico read() serve per creare ed inizializzare un array di int:
	1- chiede all'utente la dimensione n dell'array da creare, obbligando l'utente a inserire un intero;
	2- crea un array di dimensione n;
	3- assegna agli elementi dell'array valori int letti da tastiera (obbligando l'utente a inserire solo int);
	4- restituisce il riferimento all'array creato.
*/
	public static int [] read()
	{
		Scanner tastiera= new Scanner (System.in);
		System.out.println("Dimensione?");
		while(!tastiera.hasNextInt())
		{
			tastiera.next();
			System.out.println("Errore");
		}
		int dim=tastiera.nextInt();
		int [] nuovoarray=new int [dim];
		System.out.println("Inserisci elementi");
		for (int i=0;i<dim;i++)
		{
			while(!tastiera.hasNextInt())
			{
				tastiera.next();
				System.out.println("Errore");
			} 
			nuovoarray[i]=tastiera.nextInt();
		}
		return nuovoarray; 
	}

	public static void toString(int [] a)
	{
		for(int i=0;i<a.length;i++)
			System.out.print(a[i]+" ");
		System.out.println();
	}
//double
	public static double [] readDoubleArray()
	{
		Scanner tastiera= new Scanner (System.in);
		System.out.println("Dimensione?");
		while(!tastiera.hasNextInt())
		{
			tastiera.next();
			System.out.println("Errore");
		}
		int dim=tastiera.nextInt();
		double [] nuovoarray=new double [dim];
		System.out.println("Inserisci elementi");
		for (int i=0;i<dim;i++)
		{
			while(!tastiera.hasNextDouble())
			{
				tastiera.next();
				System.out.println("Errore");
			} 
			nuovoarray[i]=tastiera.nextDouble();
		}
		return nuovoarray;
	}

	public static void println(double [] a)
	{
		for(int i=0;i<a.length;i++)
			System.out.print(a[i]+" ");
		System.out.println();
	}
//String
	public static String[] readStringArray()
	{
		Scanner tastiera= new Scanner (System.in);
		System.out.println("Dimensione?");
		while(!tastiera.hasNextInt())
		{
			tastiera.next();
			System.out.println("Errore");
		}
		int n=tastiera.nextInt();
		String [] nuovoarray = new String [n];
		System.out.println("Inserisci elementi");
		tastiera.nextLine();
		for (int i=0;i<=n;i++)
			nuovoarray[i]=tastiera.nextLine();
		return nuovoarray; 
	}

	public static void println(String[] a)
	{
		for (int i=0;i<a.length;i++)
			System.out.println(a[i]+ " ");
		System.out.println();
	}
}
/*, che:
1- chiede all'utente la dimensione n dell'array da creare, obbligando l'utente a inserire un intero;
2- crea un array di String (String[]) di dimensione n;
3- assegna agli elementi dell'array valori String letti da tastiera;
4- restituisce il riferimento all'array creato.*/


/***************************************
public static void println(String[] a), che visualizza a video il contenuto di un array di String*/
