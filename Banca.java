public class Banca
{
	//definizione variabili
	private ContoCorrente [] conti;
	private int NumCont;

	//costruttore
	public Banca()
	{
		conti=new ContoCorrente [10];
		NumCont=0;
	}

	//Inserimento di un conto nell'array dei conti previa verifica dello spazio
	public void InserConto(double saldo,String Propr)
	{
		if(NumCont==conti.length)
		{
			ContoCorrente [] conti2= new ContoCorrente [(NumCont + 10)];
			conti=conti2;
		}
		NumCont ++;
		ContoCorrente one =new ContoCorrente(saldo,Propr,NumCont);
		conti [NumCont-1]=one;
	}

	//Trasferimento di una cifra da un conto ad un altro
	public void Trasferimento(ContoCorrente x, ContoCorrente y, double cifra)
	{
		x.Prelievo(cifra);
		y.Versamento(cifra);
	}

	//Ricerca del nome del proprietario nell'array dei conti
	public CoppiaXbanca RicercaConto(String x)
	{
		CoppiaXbanca result;
		boolean flag=false;
		int i=0;
		while(i<NumCont && flag==false)
		{
			if((conti[i].GetPropr()).compareToIgnoreCase(x)==0)
				flag=true;
			i++;
		}
		result=new CoppiaXbanca (flag,conti[i]);
		return result;
	}
	
	public void SortNumConto()
	{
		ContoCorrente [] Conti= new ContoCorrente [conti.length];
		Conti=MergeSort.split(conti);
		conti=Conti;
	}
}
