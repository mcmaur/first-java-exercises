import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Beeper extends JFrame
{

   private JButton button;
   private JButton bReset;
   private JPanel panel;
   private JLabel mylabel;
   public int countClick;

   Beeper()
   {
   	countClick=0;
		setTitle( "Non suona solo sul mio");
      setSize(400,300);
      setLocation(450,250);
      
      panel = new JPanel();   
		panel.setBackground(new Color(255,0,0));

      button = new JButton("Click Me");
		button.addActionListener(new BeepListener());

      bReset= new JButton("Reset");
		bReset.addActionListener(new BeepListener());
     
      mylabel = new JLabel();
	  	mylabel.setText(Integer.toString(countClick));
	  	mylabel.setVisible(true);
	  	
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      panel.add(button);
      panel.add(mylabel);
      panel.add(bReset);
      add(panel);
   }

   public static void main(String[] args)
   {
      Beeper beep = new Beeper();
      beep.setVisible(true);
   }

  private class BeepListener implements ActionListener
  {
	public void actionPerformed(ActionEvent e)
	{
		String who = e.getActionCommand();
		if(who.equals("Click Me"))
		{
		  countClick ++;
		  mylabel.setText(Integer.toString(countClick));
		  Toolkit.getDefaultToolkit().beep();
		}
		else
		{
		  countClick=0;
		  mylabel.setText(Integer.toString(countClick));
		}
	}
  }
  
}
