import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import static java.awt.GraphicsEnvironment.*;

public class BinaryTree {

  private class Node {
    Integer element; // Integer invece di int solo per il codice per il disegno
    Node left, right;

    Node(int element) {
      this.element = element;
      left = right = null;
    }

    Node(int element, Node left, Node right) {
      this.element = element;
      this.left = left;
      this.right = right;
    }

    boolean isLeaf() {
      return left == null && right == null;
    }
  }

  private Node root;

  public BinaryTree() {
    root = null;
  }

  public void display() {
    display(root, 0);
  }

  private void display(Node node, int k) {
    if(node != null) {
      display(node.right, k+1);
      for(int i = 0; i < k; i++) System.out.print("   ");
      System.out.println(node.element);
      display(node.left, k+1);
    }
  }

  public boolean isEmpty() {
    return root == null;
  }

  public void add(int element, String path) {
    if(root == null)
      root = new Node(element);
    else add(element, path, root);
  }

  private void add(int element, String path, Node node) {
    if(path.length() == 0)
        throw new IllegalArgumentException();
    char direction = path.charAt(0);
    String nextPath = path.substring(1);
    if(direction == 'L') {
      if(node.left == null) node.left = new Node(element);
      else add(element, nextPath, node.left);
    }
    else if(direction == 'R') {
      if(node.right == null) node.right = new Node(element);
      else add(element, nextPath, node.right);
    }
    else throw new IllegalArgumentException();
  }

  public void printPreOrder() {
    printPreOrder(root);
  }

  private void printPreOrder(Node node) {
    if(node != null) {
      System.out.println(node.element);
      printPreOrder(node.left);
      printPreOrder(node.right);
    }
  }

  public void printInOrder() {
	System.out.println(root.element);    
	printInOrder(root);
  }

  private void printInOrder(Node node) {
	if(node != null) {
		printPreOrder(node.left);      
		System.out.println(node.element);
		printPreOrder(node.right);
	}
  }

  public void printPostOrder() {
    printPostOrder(root);
  }

  private void printPostOrder(Node node) {
	if (node!=null) 
	{
			printPostOrder(node.left);
			printPostOrder(node.right);
			System.out.println(node.element);
	}	
  }

  /* altezza dell'albero */
  private int height(Node node) {
    if(node == null) return -1;
    else return 1 + Math.max(height(node.left), height(node.right));
  }

  public int height() {
    return height(root);
  }

  /** sintassi alternativa, con il costrutto condizionale  */

  private int height1(Node nd) {
    return nd == null ? -1 : 1 + Math.max(height1(nd.left), height1(nd.right));
  }

  public int height1() {
    return height1(root);
  }

  /* somma di tutti i valori contenuti nei nodi dell'albero */
  public int somma() {
    return somma(root);
  }

  private int somma(Node nd) {
	if(nd!=null)
	{
    	return nd.element + somma(nd.left)+ somma(nd.right);
	}
	return 0;
  }

	public int numNodi() {
	if(root!=null)
		return numNodi(root.left)+numNodi(root.right)+1;
	return 0;
	}

   //numero dei nodi dell'albero 
  public int numNodi(Node node) {
  if(root!=null)
	{
		return 1+numNodi(node.left)+numNodi(node.right);
	}
	return 0;
  }

  ...

  public int numFoglie() {
	return numFoglie(root);
  }

	public int numFoglie(Node node) {
    if (node!=null)
	{
		if(node.isLeaf())
			return 1;
		else
		{
			int i=numFoglie(node.left);
			int z=numFoglie(node.right);
		}
		return i+z;
	}
	return 0;
  }	

  ...

   //modifica l'albero trasformandolo nel suo speculare 
  public void reflect() {
    reflect(root);
  }

  private void reflect(Node node) {
	if(node!=null)
	{
	Node temp= node.left;
   node.left=node.right;
	node.right=temp;
	reflect(node.left);
	reflect(node.right);
	}
  }

  public boolean equals(BinaryTree bt) {
	if(root!=null)
		return areEqual(root.left,root.right)
	else
		return true;
  }

  private boolean areEqual(Node nd1, Node nd2) {
	if(nd1!=null && nd2!=null)
	{
		if(nd1!=null || nd2!=null)
		{
			if(nd1.element==nd2.element)
			{
				if(!nd1.isLeaf())
				boolean bol1=areEqual(nd1.left,nd2.left);
				if(!nd2.isLeaf())
				boolean bol2=areEqual(nd2.right,nd1.right);
				return bol1==bol2;
			}
			else
				return false;
		}
		return false;
	}
	return true;
  } 

   //crea e restituisce un nuovo albero uguale a this;nota bene: il nuovo albero non condivide i nodi con this,bensì è costituito di nodi nuovi  
  public BinaryTree copy() {
   if(root!=null)
	{
		BinaryTree BinaryTreeCopy=new BinaryTree();
		copy(root);
	}
	else
   System.out.println("Impossibile");
  }

  private Node copy(Node nd) {
	if(nd!=null)
	{
	Node pappo= new Node (nd.element);
	pappo.left=copy(nd.left);
	pappo.right=copy(nd.right);
	}
  }

/***************** CODICE PER DISEGNARE L'ALBERO **********************
 ****** DA COPIARE SENZA CERCARE DI CAPIRNE IL FUNZIONAMENTO ***********/

  public void draw() {
    JFrame frame = new JFrame();
    GraphicsEnvironment ge = getLocalGraphicsEnvironment();
    Rectangle bounds = ge.getMaximumWindowBounds();
    int x = bounds.x + bounds.width/4;
    int y = bounds.y + bounds.height/4;
    frame.setBounds(x,y,2*bounds.width/3,2*bounds.height/3);
    frame.add(new DrawingPanel());
    frame.setVisible(true);
  }

  private class TreeDrawing<E> {
    E element;
    TreeDrawing left, right;
    int x, y, width, height;

    TreeDrawing(E element, int x, int y, int width, int height, TreeDrawing left, TreeDrawing right) {
      this.element = element;
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
      this.left = left;
      this.right = right;
    }
  }

  private class DrawingPanel extends JPanel {
    private int visitedNodeNumber = 1;
    private Graphics graphics;
    private int hScale = 40;
    private int vScale = 40;

    TreeDrawing build(Node node, int level) {
      if(node==null) return null;
      else {
        TreeDrawing left = build(node.left, level + 1);
        String str = node.element.toString();
        Rectangle2D rect = graphics.getFontMetrics().getStringBounds(str, graphics);
        int width = (int)Math.round(rect.getWidth())+2;
        int height = (int)Math.round(rect.getHeight());
        int dx = (int)Math.round(rect.getCenterX());
        int dy = (int)Math.round(rect.getCenterY());
        int x = hScale*visitedNodeNumber - dx;
        int y = vScale*level + dy;
        visitedNodeNumber++;
        TreeDrawing right = build(node.right, level + 1);
        return new TreeDrawing<Integer>(node.element, x, y, width, height, left, right);
      }
    }

    void draw(TreeDrawing td) {
      if(td != null) {
        String str = td.element.toString();
        graphics.drawRect(td.x, td.y, td.width, td.height);
        graphics.drawString(str, td.x+1, td.y + td.height - 2);
        if(td.left != null) {
          int x1 = td.x + td.width/2;
          int y1 = td.y + td.height;
          int x2 = td.left.x + td.left.width/2;
          int y2 = td.left.y;
          graphics.drawLine(x1, y1, x2, y2);
        }
        draw(td.left);
        if(td.right != null) {
          int x1 = td.x + td.width/2;
          int y1 = td.y + td.height;
          int x2 = td.right.x + td.right.width/2;
          int y2 = td.right.y;
          graphics.drawLine(x1, y1, x2, y2);
        }
        draw(td.right);
      }
    }


    public void paintComponent(Graphics g) {
      super.paintComponent(g);
      graphics = g;
      TreeDrawing td = build(root, 1);
      draw(td);
      visitedNodeNumber = 1;
    }
  }
/***********  FINE CODICE PER DISEGNARE L'ALBERO ******************/
 
  public static void main(String[] args) {
    BinaryTree myTree = new BinaryTree();
    myTree.add(100, "");
    myTree.add(10, "L");
    myTree.add(20, "R");
    myTree.add(30, "LL");
    myTree.add(40, "RL");
    myTree.add(50, "RR");
    // ...
    myTree.display();
    myTree.reflect();
    myTree.display();
    myTree.draw();
    BinaryTree yourTree = myTree.copy();
    yourTree.display();
    yourTree.draw();
    // ...
  }

}



