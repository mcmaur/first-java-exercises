import java.util.*;
public class BubbleSort
{
	public static void main (String[]args)
	{
		Scanner tastiera=new Scanner (System.in);
		System.out.print("Inserisci numero componenti del vettore ");
		int dim=tastiera.nextInt();
		int array []=new int [dim];
		System.out.println();
		System.out.println("Inserisci valori");
		for (int i=0;i<dim;i++)
		{
			array[i]=tastiera.nextInt(); 
		}
		for (int i=array.length -1;i>0;i--)
		{
			for (int j=0;j<i;j++)
			{
				if(array[j]>array[j+1])
				{
					int z=array[j];
					array [j]=array[j+1];
					array[j+1]=z;
				}
			}

		}
		for(int i=0;i<array.length;i++)
		{
			System.out.print(array [i] + " ");
		}
		System.out.println();
	}
}
