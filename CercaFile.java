import java.io.File;
import java.util.Scanner;


public class CercaFile {
	
	public static void main (String args[]) {
		Scanner tast = new Scanner(System.in);
		System.out.println("Immetti il nome del file da cercare");		
		String nomeFile=tast.nextLine();
		System.out.println("Immetti il nome del direttorio in cui cercare");		
		String nomeDir=tast.nextLine();
		
		if (cercaFile(nomeDir,nomeFile)!=null)
			System.out.println("file trovato");
		else
			System.out.println("file non trovato");
	}

	static File cercaFile(String cartella, String nome){
		return cercaFile(new File(cartella),nome);
	}

	private static File cercaFile(File cartella,String nome){
		File [] files = cartella.listFiles();
		if (files==null) return null;
		for (File file:files)
		{
			if(file.isDirectory()){
				File trov=cercaFile(file,nome);
				if (trov!=null) return trov;
			}
			else if (file.getName().equals(nome)) return file;
		}
		return null;
	}
}

