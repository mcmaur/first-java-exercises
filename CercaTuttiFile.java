/*Si scriva una classe di nome CercaFile con un metodo che dato il nome di una directory e il nome di un file cerca nel direttorio indicato e ricorsivamente nei suoi sottodirettori tale file. Stampa la locazione della prima occrrenza trovata. Scrivere una versione che stampi il file appena trovato e una che restituisca in un oggetto di tipo File il puntatore (al file trovato e stampi successivamente il cammino a tale file dal direttorio indicato inizialmente.

La scansione della directory deve essere implementata in modo ricorsivo.

Per ottenere le informazioni su una directory (ad es. i file e le directory in essa contenute) e su ogni singolo file si utilizzi la classe File. Per avere informazioni su tale classe si utilizzi l'help di Java, http://java.sun.com/javase/6/docs/api/. In particolare, si considerino i metodi: listFiles, isFile, getName, isDirectory, getPath.

Partendo dalla classe CercaFile, scrivere una classe CercaTuttiFile che dato il nome di una directory e il nome di un file cerca nel direttorio indicato e ricorsivamente nei suoi sottodirettori tutte le occorrenze di tale file e ne stampi la locazione. Scrivere una versione che stampi i file appena trovati e una che restituisca in un oggetto di tipo vector tutti i puntatori (di tipo File) ai file trovati e stampi successivamente il cammino a tali file dal direttorio indicato inizialmente.

Scrivere una classe FileSystemStatistics con un metodo statistics che dato il nome di un direttory restituisce una tripla di interi (int[]) che contiene:

1- la profondità di annidamento massima in termini di direttori;
2- il numero di file totali del direttory indicato e ricorsivamente di tutti i suoi sotto direttori;
3- il numero di direttori totali del direttory indicato e ricorsivamente di tutti i suoi sotto direttori.*/

import java.util.*;
import java.io.*;
public class CercaTuttiFile
{
	private static Vector <File> Risultati=new <File> Vector();

	public static void cercaTuttiFile(File currentDirectory,String fileDaCercare)
	{
		int i=0;
		//Vector <File> Risultati=new <File> Vector();
		File FileTrovato=null;
		File [] listaFile = currentDirectory.listFiles();
		while(i<listaFile.length)
		{
			if(listaFile[i].isFile())
			{
				if(listaFile[i].getName().compareToIgnoreCase(fileDaCercare)==0)
				{
					Risultati.add(listaFile[i]);
				}
			}	
			else
			{
				if(listaFile[i].isDirectory())
				{
					cercaTuttiFile(listaFile[i],fileDaCercare);
				}
				
			}
			i++;
		}
	}

	public static void main (String[]args)
	{
		String fileDaCercare=UtilLeggiTastiera.leggiStringaDaTastiera("File da cercare? ");
		String directoryDiPartenz=UtilLeggiTastiera.leggiStringaDaTastiera("In quale directory? ");
		File directoryDiPartenza = new File (directoryDiPartenz);
		cercaTuttiFile (directoryDiPartenza,fileDaCercare);
		for (int i=0;i<Risultati.size();i++)
	}
}
