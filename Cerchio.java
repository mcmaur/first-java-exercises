public class Cerchio implements Figura 
{
	private double r;
	public Cerchio(double r)
	{
		this.r = r;
	}
	public double area()
	{
		return Math.PI*r*r;
	}
	public double perimetro()
	{
		return 2*Math.PI*r;
	}
	public String toString()
	{
		return("Sono un cerchio. Le mie misure sono:\n area: "+area()+"\n perimetro: "+perimetro());
	}
}
