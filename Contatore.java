public class Contatore
{
	private int cont;

	public Contatore()
	{
		cont=0;
	}

	public Contatore (int x)
	{
		cont=x;
	}

	public int init (int i)
	{
		cont=i;
		return cont;
	}

	public int getValore ()
	{
		return cont;
	}

	public int incr ()
	{
		cont ++;
		return cont;
	}

	public int decr ()
	{
		cont --;
		return cont;
	}

	public int incr (int a)
	{
		cont=cont + a;
		return cont;
	}

	public int decr (int a)
	{
		cont=cont - a;
		return cont;
	}
}
