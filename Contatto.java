/**

La classe Contatto.java permette di definire oggetti di tipo Contatto, che descrivono
i dati essenziali di persone.
La classe NON CONTIENE MAIN (non e' un'applicazione, bensi' un generatore di oggetti!!).
Per verificare se avete sviluppato correttamente la classe, utilizzate un'applicazione
separata. Per esempio, la classe ProvaContatto.java.

 */

import java.util.Scanner;
public class Contatto 
{
	// Variabili di istanza (prima quelle public, se ci sono, poi quelle private)
	private String nome;
	private String cognome;
	private int eta;
	// TO-DO: aggiungere una variabile di istanza boolean patente (che specifica se la persona
	// descritta dal contatto ha la patente o meno
 	// TO-DO: aggiungere una variabile di istanza numeroDiTelefono
	private boolean patente;
	private String numeroDiTelefono;

	/*
    COSTRUTTORE 1: inizializza le variabili di istanza con valori "di base".
    Non ha parametri.
    */
	public Contatto() 
	{
        nome = "";
        cognome = "";
        eta = 0;
		patente=false;
		numeroDiTelefono="0";
        /*...completare l'inizializzazione delle variabili di istanza */
    }


   // TO-DO: Sviluppare il COSTRUTTORE 2
   // che ha 5 parametri (2 String, 1 int, 1 boolean,  ...) che rappresentano i dati di un contatto.
   // Il costruttore crea l'oggetto e inizializza le variabili di istanza come
   // specificato dai parametri
	public Contatto(String nome,String cognome,int eta,boolean patente,String numeroDiTelefono) {
		this.nome=nome;
		this.cognome=cognome;
		this.eta=eta;
		this.patente=patente;
		this.numeroDiTelefono=numeroDiTelefono;
	}

    // METODI DI ISTANZA


    /**
      TO-DO: sviluppare i seguenti metodi, che restituiscono come output i valori
      delle variabili di istanza di "this":

      public String getNome()
      public String getCognome()
      ... getEta()
      ... getPatente()
      ... getNumeroDiTelefono()

      NB: questi metodi hanno un tipo di output che dipende dal valore da restituire (int, String, o altro).
          Inoltre, non hanno parametri in input perche' recuperano il valore da restituire direttamente
          dallo stato dell'oggetto "this"
     */

    //  restuisce il valore della variabile nome
    public String getNome() {
		return this.nome;
    }

    // restituisce il valore della variabile cognome
    public String getCognome() {
		return this.cognome;
    }


    // restituisce il valore della variabile eta
    public int getEta() {
        return this.eta;
    }

    // restituisce il valore della variabile patente
    public boolean getPat() {
        return this.patente;
    }

    // restituisce il valore della variabile numeroDiTelefono
     public String getNumeroDiTelefono(){
        return numeroDiTelefono;
     }

    /**
      TO-DO: sviluppare i seguenti metodi, che assegnano alle variabili di istanza di "this"
      i valori ricevuti come parametro:

      public void setNome(String n)
      public void setCognome(String c)
      ... setEta...
      ... setPatente...
      ... setNumeroDiTelefono...

      NB: questi metodi sono tutti void perche' non devono restituire alcun tipo di valore.
     */
    // modifica il valore della variabile nome
    public void setNome(String n) {
        this.nome=n;
    }

    // modifica il valore della variabile cognome
    public void setCognome(String n) {
        this.cognome=n;
    }

    // modifica il valore della variabile eta
    public void setEta(int n) {
        this.eta=n;
    }

    // modifica il valore della variabile patente
    public void setPat(boolean p) {
          this.patente=p;
    }

    //modifica il valore della variabile numeroDiTelefono
    public void setNumeroDiTelefono(String n){
           this.numeroDiTelefono=n;
    }
	
    /** public void riempiContatto()
        Chiede all'utente i dati di un contatto (nome, cognome, etc.) ed assegna alle variabili di istanza dell'oggetto "this" i valori di tali dati.
    */
    public void riempiContatto() {
        Scanner tastiera = new Scanner(System.in);
        System.out.print("Nome: ");
        this.nome = tastiera.nextLine();/* NB: "nome" e' la var di istanza di "this". */
        /* completare il metodo */
		System.out.print("Cognome: ");
		this.cognome=tastiera.nextLine();
		System.out.print("Numero telefonic: ");
		this.numeroDiTelefono=tastiera.nextLine();
		System.out.print("eta: ");
		this.eta=tastiera.nextInt();
		System.out.print("Patente: ");
		this.patente=tastiera.nextBoolean();
    }

    /** public void modificaContatto()
        Chiede all'utente i dati di un contatto che vanno modificati (nome, cognome, etc.) ed aggiorna le variabili di istanza dell'oggetto "this" i valori di tali dati.
    */
    public void modificaContatto() {
      String ris = "";
      Scanner tastiera = new Scanner(System.in);
      System.out.println("Vuoi modificare il nome? (per confermare premi s+invio, altrimenti qualsiasi altro tasto)");
      ris=tastiera.next();
      tastiera.nextLine();
      if (ris.equals("s")) 
	  {
            System.out.println("Inserisci il nuovo nome (seguito da invio)");
            String in = tastiera.nextLine();
            this.nome = in;
	  }
	  System.out.println("Vuoi modificare il cognome? (per confermare premi s+invio, altrimenti qualsiasi altro tasto)");
      ris=tastiera.next();
      tastiera.nextLine();
      if (ris.equals("s")) 
	  {
            System.out.println("Inserisci il nuovo cognome (seguito da invio)");
            String ip = tastiera.nextLine();
            this.nome = ip;
	  }
	  System.out.println("Vuoi modificare l' eta? (per confermare premi s+invio, altrimenti qualsiasi altro tasto)");
      ris=tastiera.next();
      tastiera.nextLine();
      if (ris.equals("s")) 
	  {
            System.out.println("Inserisci la nuovo eta (seguito da invio)");
            String iw = tastiera.nextLine();
            this.nome = iw;
	  }
	  System.out.println("Vuoi modificare la patente? (per confermare premi s+invio, altrimenti qualsiasi altro tasto)");
      ris=tastiera.next();
      tastiera.nextLine();
      if (ris.equals("s")) 
	  {
            System.out.println("Inserisci la nuova patente (seguito da invio)");
            String is = tastiera.nextLine();
            this.nome = is;
      }
	  System.out.println("Vuoi modificare il numero telefonico? (per confermare premi s+invio, altrimenti qualsiasi altro tasto)");
      ris=tastiera.next();
      tastiera.nextLine();
      if (ris.equals("s")) 
	  {
            System.out.println("Inserisci il nuovo numero telefonico (seguito da invio)");
            String iy = tastiera.nextLine();
            this.nome = iy;
      }
      /* fare la stessa cosa per tutte le variabili di istanza */
  }

    /*
      public String toString()
      restituisce una stringa contenente i valori di tutte le variabili dell'oggetto, opportunamente concatenate.
    Il metodo toString() e' molto utile perche' permette di restituire lo stato di un oggetto sotto forma di Stringa, facilmente visualizzabile a video (senza dover fare la "get" di tutte le variabili dell'oggetto).
    */
    public String toString()
	{
		return " Nome: "+this.nome +" Cognome: "+this.cognome+ " Eta: "+this.eta+ " Patente: "+this.patente+ " Numero Telefonico: "+this.numeroDiTelefono;
	}

  /** public boolean etaMaggiore(int e)
      prende come parametro un valore int e lo confronta con l'eta` di questo contatto.
    Il metodo restituisce true se "this" ha eta' maggiore del valore "e", false altrimenti.
    */
    public boolean etaMaggiore(int e) 
	{
    	if(this.eta>e)
			return true;
		else
			return false;
    }

	
    /** aggiungere un metodo booleano confrontaCognome(...) che confronta il cognome del contatto con quello di un altro Contatto passato come parametro del metodo.
    Il metodo restituisce true se i due contatti hanno cognome uguale; false altrimenti
      */
      public boolean confrontaCognome(String e) 
	  {
      	if(this.cognome.compareTo(e)==0)
			return true;
		else
			return false;
			
      }


    /** aggiungere un metodo confrontaNomeCognome(...)
        che confronta il cognome ed il nome del contatto con quello di un altro Contatto passato come parametro del metodo.
    Il metodo restituisce true se i due contatti hanno cognome e nome uguali; false altrimenti
      */
	public boolean confrontaNomeCognome(String nome,String cognome) 
	{
   		if(this.cognome.compareTo(cognome)==0 && this.nome.compareTo(nome)==0)
			return true;
		else
			return false;	
    }
} // end Contatto
