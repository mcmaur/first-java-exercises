public class ContoCorrente
{
	//dichiarazione variabile
	private double saldo;
	private String Propr;
	private int NumConto;

	//costruttore
	public ContoCorrente(double saldo, String Propr,int num)
	{
		this.saldo=saldo;
		this.Propr=Propr;
		NumConto=num;
	}

	// Attuazione del versamento previa verifica disponibilità
	public boolean Versamento (double x)
	{
		if (saldo<=x)
		{
			saldo=saldo + x;
			return true;
		}
		else return false;
	}

	// Attuazione del prelievo previa verifica disponibilità
	public boolean Prelievo (double x)
	{
		if (saldo<=x)
		{
			saldo=saldo - x;
			return true;
		}
		else return false;
	}

	// return del saldo
	public double GetSaldo()
	{
		return saldo;
	}

	// return del nome del proprietario
	public String GetPropr()
	{
		return Propr;
	}

	//return del numero del conto
	public int GetNumConto()
	{
		return NumConto;
	}

}
