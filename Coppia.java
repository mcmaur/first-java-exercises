/** Gli oggetti di questa classe sono coppie, composte di un intero e una stringa.
 */

public class Coppia {

  private int i;
  private String s;



  // NON INTRODURRE NESSUN COSTRUTTORE!
  // Si usa il costuttore di default.


 public int getI() {

  return i;
 }

 public String getS() {

  return s;
 }


 public void setI(int i) {

  this.i = i;
 }

 public void setS(String s) {

  this.s = s;
 }


 /**Da completare il metodo toString che metta sotto forma di stringa i valori dell'oggetto.*/
 public String toString()
 {
	return "i: "+this.getI()+ " s: "+ this.getS();
 }


}
