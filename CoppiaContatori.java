public class CoppiaContatori
{
	private Contatore cont1;
	private Contatore cont2;

	public CoppiaContatori()
	{
		cont1=new Contatore();
		cont2=new Contatore();
	}

	public CoppiaContatori(int x, int y)
	{
		cont1=new Contatore(x);
		cont2=new Contatore(y);
	}
	
	public CoppiaContatori(Contatore x, Contatore y)
	{
		cont1=x;
		cont2=y;
	}

	public Contatore first()
	{
		return cont1;
	}

	public Contatore second()
	{
		return cont2;
	}

	public static void main (String [] args)
	{
		CoppiaContatori cont = new CoppiaContatori(2,3);
		System.out.println(cont.first().getValore());
		System.out.println(cont.second().getValore());
		cont.first().init(1);
		System.out.println(cont.first().getValore());
		cont.second().incr();
		System.out.println(cont.second().getValore());
		cont.first().decr(3);
		System.out.println(cont.first().getValore());
	}

}
