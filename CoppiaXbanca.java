public class CoppiaXbanca
{
	private boolean bol;
	private ContoCorrente cont;

	public CoppiaXbanca(boolean bol,ContoCorrente cont)
	{
		this.bol=bol;
		this.cont=cont;
	}

	public ContoCorrente getConto()
	{
		return cont;
	}

	public boolean getBoolean()
	{
		return bol;
	}
}
