import java.util.*;

class ChopStick
{
	boolean available;
	ChopStick()
	{
		available = true;
	}
	public synchronized void takeup()
	{//acquire(lock)
		while(!available)
		{
			try
			{
				wait();
			}catch(InterruptedException ie)
			{
				ie.getStackTrace();
			}
		}
		available=false;
	 //release (lock)
	}
	public synchronized void putdown()
	{
		while(available)
		{
			try
			{
				wait();
			}
			catch(InterruptedException ie)
			{
				ie.getStackTrace();
			}
		}
		available = true;
		notifyAll();
	}
}

class Philosopher extends Thread
{
	ChopStick left,right;
	int philo_num;

	Philosopher(int num, ChopStick chop1, ChopStick chop2){
		philo_num = num;
		left = chop1;
		right = chop2;
	}
	public void eat()
	{
		left.takeup();
		right.takeup();
		System.out.println("Philosopher " +(philo_num+1)+ " is eating");
	}

	public void think(){
		left.putdown();
		right.putdown();
		System.out.println("Philospher " +(philo_num+1)+ " is thinking");
	}

	public void run()
	{
		activate();
	}

	public void activate(){
		Random rnd = new Random();
		while(true){
			eat();
			try{
				Thread.sleep(rnd.nextInt(3000));
			}catch(InterruptedException e){}
			think();
			try{
				Thread.sleep(rnd.nextInt(3000));
			}catch(InterruptedException e){}
		}
	}

}

class Dining
{
	static ChopStick[] chopsticks = new ChopStick[5];
	static Philosopher[] philos = new Philosopher[5];
	
	public static void main(String args[])
	{
		for(int count = 0; count <= 4; count++){
			chopsticks[count] = new ChopStick();
		}

		for(int count = 0; count <= 4; count++){
			if(count==0)
				philos[count] = new Philosopher(count,chopsticks[(count+1)%5],chopsticks[count]);
			else philos[count] = new Philosopher(count, chopsticks[count],chopsticks[(count+1)%5]);
		}

		for(int count = 0; count <= 4; count++)
			philos[count].start();
	}

}
