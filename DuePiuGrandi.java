/*****************
Definire una classe DuePiuGrandi il cui main:
- legge da tastiera una sequenza di numeri interi costituita da almeno DUE elementi e terminata da un non intero
- visualizza a video i due valori più grandi (che possono anche essere uguali). Per semplicità, non si controlli che vengano effettivamente immessi almeno due valori (oppure ispirarsi ai suggerimenti degli esercizi precedenti).

HELP: si definiscano due variabili, max e viceMax, che rappresentano, rispettivamente il valore piu' grande, ed il secondo valore piu' grande trovati nella sequenza al passo i-mo.
- Le due variabili vengono inizializzate con i valori dei primi due interi letti da tastiera (mettendo il piu' grande in max e l'altro in viceMax).
- Poi si deve ciclare leggendo nuovi valori ed aggiornando max e viceMax opportunamente

NB: attenzione agli aggiornamenti di viceMax: se il numero inserito dall'utente e' compreso tra viceMax e max, viceMax deve essere aggiornato.
Es: data la sequenza  2  8  5  -->  max=8, viceMax=5*/

import java.util.Scanner;
public class DuePiuGrandi
{
	public static void main (String[]args)
	{
	Scanner tastiera= new Scanner(System.in);
	int m=0,n=0,k=0,max=0,vicemax=0;
	n=tastiera.nextInt();
	m=tastiera.nextInt();
	if (n>m)
		{
		max=n;
		vicemax=m;
		}
	else
		{
		max=m;
		vicemax=m;
		}	
	while (tastiera.hasNextInt())
	{
	
	k=tastiera.nextInt();
	if (k>max)
		 {
		vicemax=max;
		max=k;
		}
	else
		if (k>vicemax)
			vicemax=k;
	}
	System.out.println("Il numero massimo della sequenza equivale ad " + max +" mentre il numero immediamente più grande dopo il max equivale ad " + vicemax);
    }
}
