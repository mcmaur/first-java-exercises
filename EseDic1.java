/** Questa classe usa i metodi readStringArray() e
    println(String[] a) di ArrayUtil.
    Controllare la corrispondenza dei nomi di questi metodi coi nomi
    dei metodi della VOSTRA classe ArrayUtil.

    Usa inoltre la classe Coppia.
 */

import java.util.*;
import static java.lang.System.*;

public class EseDic1{

 public static void main(String [] args) {

  Scanner tastiera = new Scanner(in);


 // (1) Dichiarare un array di stringhe,chiamato arraystringhe, e riempirlo
 // con parole a piacere
 // usando il metodo readStringArray() di ArrayUtil.


 // (2) Richiamare opportunamente il metodo che avrete completato qui sotto
 // e stamparne il risultato, stampando ANCHE  l'array con l' opportuna
 // println.



 } // fine main


 /** (3) Definire il  metodo statico indicato qui sotto.
  */


/** Il metodo parolaPiuFrequente() prende come argomento un array di stringhe,
    conta le occorrenze delle stringhe contenute nell'array (cioe` conta il
    numero di volte in cui una stringa compare nell'array) e restituisce una coppia
    formata dalla stringa che ha il maggior numero di occorrenze *contigue* e dal
    numero di occorrenze della stringa stessa.
    Se tutte le stringhe hanno lo stesso numero di occorrenze il metodo restituisce
    la coppia <"", 0>.
    Ad esempio, dato l'array <"bianco", "blu", "blu", "rosso", "rosso", "rosso", "verde">,
    il metodo restituira' la coppia <"rosso", 3>.
    -Suggerimento-
     Dovete utilizzare quattro variabili, la prime due servono rispettivamente per memorizzare
     la stringa correntemente esaminata e per contare via via le sue occorrenze, le seconde due
     servono rispettivamente per memorizzare la stringa che occorre di piu` fino a quel momento e
     il numero di volte in cui occorre. Alla fine, nelle seconde due avremo il risultato che va
     messo dentro la Coppia risultato.
   */
 public static Coppia parolaPiuFrequente(String [] r) {}

}
