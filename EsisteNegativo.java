/*Sviluppare la classe EsisteNegativo.java che:

- crea un array di interi chiedendo all'utente la dimensione dell'array

- inizializza l'array chiedendo all'utente i valori da tastiera

- visualizza a video (con ciclo FOR) il contenuto dell'array

- verifica (con un ciclo WHILE) se l'array contiene almeno un numero negativo.

- visualizza a video la scritta "c'e' almeno un numero negativo" in caso positivo, "non c'e' nessun numero negativo" in caso negativo.

NB: se l'array contiene almeno un numero negativo, il ciclo while deve fermarsi appena viene trovato tale numero. Altrimenti, analizza tutti gli elementi dell'array.*/


import java.util.*;
public class EsisteNegativo
{
	public static void main (String[]args)
	{
	int [] array;
	int n=0,k=0;
	boolean trovato = false;
	Scanner tastiera = new Scanner(System.in);
	System.out.println("Inserisci numero componenti dell'array");
	n=tastiera.nextInt();
	array = new int [n];
	for (int i=0;i<array.length;i++)
	{
		System.out.println("Inserisci i valori da immettere nell'array");
		array[i]=tastiera.nextInt();
	}
		while (trovato==false && k<array.length)
		{
			if (array[k]>0)
				k++;
			else
			{
				System.out.println("Esiste almeno un valore negativo");
				trovato = true;
			}
		}	
	if (trovato==false)
		System.out.println("Non esiste alcun valore negativo");	
	}
}
