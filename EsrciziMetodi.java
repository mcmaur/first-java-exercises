/*

Utilizzando metodi statici, sviluppare un'applicazione il cui main
gestisce un menu' ciclico di operazioni su due numeri interi.
Le operazioni siano identificate mediante numeri interi
(come nel seguito) e siano implementate da opportuni metodi:
1) dati due interi, n e m, il cui valore e' letto da tastiera, verifica se n e' multiplo m.
2) verifica uguaglianza di due numeri interi, e di due numeri con virgola
3) calcolare prodotto di due numeri
... altri metodi, a vostra scelta...

NB: per l'acquisizione dell'input dell'utente (operazione richiesta
e valori degli operandi) usare i metodi offerti dalla classe
GetInput (ed estenderla opportunamente per trattare altri tipi di
dati - boolean, etc.) 
NB: GetInput deve essere salvata nella stessa cartella in cui si trova EserciziMetodi****** */

import java.util.*;

public class EserciziMetodi {

	public static void main(String[] args) {
		int op = 0; // ID dell'operazione da eseguire
		int n1 = 0; // primo operando
		int n2 = 0; // secondo operando

		Scanner sc = new Scanner(System.in);

		do {
			System.out.print("Digita scelta (1, 2 o 3; o altri numeri per uscire): ");
			op = GetInput.getIntInput(sc);

			if (op==1||op==2||op==3) {
				System.out.print("Digita il primo numero intero: ");
				n1 = GetInput.getIntInput(sc);
				System.out.print("Digita il secondo numero intero: ");
				n2 = GetInput.getIntInput(sc);
			}

			if (op==1) { //verifica multipli e visualizza risultato
				boolean ris = multipli(n1, n2);
				if (ris)
					System.out.println(n1 + "e' multiplo di " + n2);
				else
				    System.out.println(n1 + "non e' multiplo di " + n2);
			}
			else if (op==2) { // verifica uguaglianza e visualizza risultato
				// inserire la chiamata dell'opportuno metodo
			}
			else if (op==3) { // calcola prodotto e visualizza risultato
				// inserire la chiamata dell'opportuno metodo
			}
			else System.out.println("Bye!");
		}
			while (op==1||op==2||op==3);
	}


	public static boolean multipli(int x, int y) {
		boolean ris = true;
		// implementare il codice del metodo
		return ris;
	}

}// end class
