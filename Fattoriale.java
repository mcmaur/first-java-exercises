/*FATTORIALE

Sviluppare una classe Fattoriale.java che permette di:
- Leggere da tastiera un numero intero "n"
- calcolare il fattoriale di "n" utilizzando un ciclo
- visualizzare a video il risultato

Successivamente, modificare la classe per calcolare il fattoriale di "n" usando un ciclo WHILE

Si ricorda che il fattoriale di n e' pari a
n*(n-1)*(n-2)*...*2*1*/

import java.util.Scanner;
public class Fattoriale
{
	public static void main (String[]args)
	{
		Scanner tastiera = new Scanner(System.in);
		int n=0,i=0,r=0;
		n=tastiera.nextInt();
		while (i<=n)
			{
			r=r+(n*(n-i));
			i++;
			}
		System.out.println("Risultato di n! = " + r);
	}
}
