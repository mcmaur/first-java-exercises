import java.io.*;
import java.util.*;
public class FileSystemStatistics
{
	public static int profondita(File currentDirectory)
	{
		int max=0;
		for(File f :currentDirectory.listFiles())
		{
			if(f.isDirectory())
			{
				int temp=profondita(f);
				if(temp>max)
					max=temp;
			}
		}
		return max+1;
	}

	public static int numFile(File currentDirectory)
	{
		int numFile=0;
		for (File f:currentDirectory.listFiles())
			if(f.isFile())
				numFile++;
			else
				if(f.isDirectory())
					numFile+=numFile(f);
		return numFile;
	}

	public static int numDirectory(File currentDirectory)
	{
		int num=0;
		for (File f:currentDirectory.listFiles())
			if(f.isDirectory())
				num+=numDirectory(f) +1;
		return num;
	}

	public static void main (String[] args)
	{
		File directoryDiPartenza = new File (args [0]);
		System.out.println("Il numero dei file: "+numFile (directoryDiPartenza));
		System.out.println("La profondità: "+profondita (directoryDiPartenza));
		System.out.println("Il numero delle sottocartelle: "+numDirectory(directoryDiPartenza));
	}
}
