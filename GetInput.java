/*

Utilizzando metodi statici, sviluppare un'applicazione il cui main gestisce un menu' ciclico
di operazioni su due numeri interi. Le operazioni siano identificate mediante numeri interi
(come nel seguito) e siano implementate da opportuni metodi:
1) dati due interi, n e m, il cui valore e' letto da tastiera, verifica se n e' multiplo m.
2) verifica uguaglianza di due numeri interi, e di due numeri con virgola
3) calcolare prodotto di due numeri
... altri metodi, a vostra scelta...

poi, separare in classe a parte i metodi di lettura di input (per es, lettura di
interi, double, etc., con controllo, per poterli invocare da piu' classi diverse)
*/

import java.util.*;

public class GetInput {


/* acquisisce da input un numero interno (e segnala errore, e permette di correggere,
se l'utente inserisce caratteri non numerici */
	public static int getIntInput(Scanner sca) {

		while (!sca.hasNextInt()) {
			System.out.println("Sbagliato!");
			sca.next();
		}
		int op = sca.nextInt();
		return op;
	}

/* acquisisce da input un numero interno (e segnala errore, e permette di correggere,
se l'utente inserisce caratteri non numerici */
	public static double getDoubleInput(Scanner sca) {

		while (!sca.hasNextDouble()) {
			System.out.println("Sbagliato!");
			sca.next();
		}
		double op = sca.nextDouble();
		return op;
	}
}
