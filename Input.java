import java.util.*;
public class Input
{
	public static int IntInput (Scanner tastiera, int a)
	{
		int input;
		input = tastiera.nextInt();
		while (!tastiera.hasNextInt())
		{
			System.out.println("errore!");
			tastiera.next();
		}
		while (tastiera.hasNextInt())
		return input;	
	}
}
