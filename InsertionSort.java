import java.util.*;
public class InsertionSort
{
	public static void main (String[]args)
	{
		int j,v;
		Scanner tastiera = new Scanner (System.in);
		System.out.print("Inserisci lunghezza vettore ");
		int dim=tastiera.nextInt();
		int [] array = new int [dim];
		System.out.println();
		System.out.println("Inserisci valori");
		for (int i=0;i<dim;i++)
		{
			array[i]=tastiera.nextInt();
		}
		for (int i=0;i<dim;i++)
		{
			v=array [i];
			j=i;
			while (j>0 && array [j-1]>v)
			{
				array[j]=array[j-1];
				j--;
			}
			array[j]=v;
		}
		for (int i=0;i<dim;i++)
		System.out.print(array[i]+ " ");
	}
}
