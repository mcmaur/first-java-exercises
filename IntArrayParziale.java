import java.util.*;
public class IntArrayParziale
{
	public boolean aggiungiElemento(int val) 
	{
		if(numElementi == elementi.length)
			return false     // l’operazione non e` andata a buon fine
		else 
		{
			elementi[numElementi] = val;
			numElementi++;
			return true;       // l’operazione e` andata a buon fine
		}
	}


	public boolean eliminaElementoDiIndice(int i)
	{
		if(i < 0 || i >= numElementi) 
		{
			return false // l’op. non e`  p ppossibile
		}
		for(int j = i+1; j < numElementi; j++) 
		{
			elementi[j-1] = elementi[j];
		}
		numElementi--;
		return true; // l’op. e` stata eseguita
	}

