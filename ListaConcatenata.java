public class ListaConcatenata {

    private class ListElem {
    
        private int dato;
        private ListElem next = null;

        public int getDato() {
            return dato;
        }
  
        public void setDato(int dato) {
            this.dato = dato;
        }

        public ListElem getNext() {
            return next;
        }
    
        public void setNext(ListElem next) {
            this.next = next;
        }

        public boolean equals(int val) {
            return dato == val;
        }

        public void draw() {
            System.out.print("["+ dato + "|");
            if (next == null)
                System.out.print("null");
            else
                System.out.print("next");
            System.out.print("]");
        }

        /* Esempi compiti */

        private ListElem reverseList(ListElem accList) {
            ListElem newAccList = new ListElem();
            newAccList.dato = this.dato;
            newAccList.next = accList;
            if (this.next != null)
                return next.reverseList(newAccList);
            else
                return newAccList;
        }

    }
    
    private ListElem first;
 
    public ListaConcatenata() {
        first = null;
    }

    public void insert(int val) {

        ListElem elem = new ListElem();
        elem.setDato(val);
        elem.setNext(first);

        first = elem;

    } 
 
    public boolean delete(int val) {

        boolean found = false;
       
        if (this.isEmpty())
	    return false;

        if (first.equals(val)) {
           // L'elemento cercato e` il primo, quindi devo
           // modifiare first
           first = first.getNext();
           found = true;
        }
        else {
           // L'elemento cercato non e` il primo, devo modificare
           // un elemento generico
           ListElem iterator = first;
           while ((iterator.getNext()) != null && !found)
               if ((iterator.getNext()).equals(val)) {
                   iterator.setNext((iterator.getNext()).getNext());
                   found = true;
               }
               else 
                   iterator = iterator.getNext();
        }
        return found; 

    }

    public boolean delete_bis(int val) {

        if (first.equals(val)) {
            // L'elemento cercato e` il primo, quindi devo
            // modifiare first
            first = first.getNext();
            return true;
        }
        else {
            // L'elemento cercato non e` il primo, devo modificare
            // un elemento generico
            ListElem iterator = first;
            while ((iterator.getNext()) != null)
                if ((iterator.getNext()).equals(val)) {
                    iterator.setNext((iterator.getNext()).getNext());
                    return true;
                }
                else 
                    iterator = iterator.getNext();
        }
        return false; 
    }

    public boolean delete_ter(int val) {

        boolean found = false;
       
        if (this.isEmpty())
	    return false;

        if (first.equals(val)) {
           // L'elemento cercato e` il primo, quindi devo
           // modifiare first
           first = first.getNext();
           found = true;
        }
        else {
           // L'elemento cercato non e` il primo, devo modificare
           // un elemento generico
           ListElem predecessor = first;
           ListElem iterator = first.getNext();
           while (iterator != null && !found)
               if (iterator.equals(val)) {
                   predecessor.setNext(iterator.getNext());
                   found = true;
               } else {
                   predecessor = iterator;
                   iterator = iterator.getNext();
               }
        }
        return found; 

    }

    public boolean isEmpty() {
        return first == null;
    }

    public boolean member(int val) {
        ListElem iterator = first;
        boolean found = false;
        while ((iterator != null) && !found) 
            if (iterator.equals(val))
                found = true; 
            else
                iterator = iterator.getNext();
        return found; 
    }

    // Alternativa all'utilizzo di found
    public boolean member_bis(int val) {
        ListElem iterator = first;
        while ((iterator != null)) 
            if (iterator.equals(val))
                return true;
            else
                iterator = iterator.getNext();
        return false; 
    }

    public void printList() {
        ListElem iterator = first;
        System.out.print("Elementi: ");
        while (iterator != null) {
            System.out.print(iterator.getDato() + ",");
            iterator = iterator.getNext();
        }
        System.out.println("");
    }  

    public void draw() {
        ListElem iterator = first;
        if (first == null)
            System.out.print("this_MiaLista-->[|first==null|]");
        else
            System.out.print("this_MiaLista-->[|first|]");
        while (iterator != null) {
            System.out.print("-->");
            iterator.draw();
            iterator = iterator.getNext();
        }
        System.out.println("");
        LJV.drawGraph(this);
    }  

    // Altri metodi un po' piu` specializzati
    
    public void insertFirst(int val) {
        insert(val);
    }
 
    public void insertLast(int val) {

        ListElem elem = new ListElem();
        elem.setDato(val);

        if (first == null) 
            first = elem;
        // Alternativa:
        // if (isEmpty()) 
        //    insertFirst(val);
        else {
            ListElem iterator = first;
            while (iterator.getNext() != null)
                iterator = iterator.getNext();
            iterator.setNext(elem);
        }
        
    }        

    public boolean deleteFirst() {

        if (first == null) 
        // Alternativa:
        // if (isEmpty()) 
            return false;
        else {
            first = first.getNext();
            return true;
        }    
        
    }

    public boolean deleteLast() {
        if (first == null)
        // Alternativa:
        // if (isEmpty()) 
            return false;
        else if (first.getNext() == null) {
            // Ha un solo elemento, devo modificare first!
            first = first.getNext();
            // Alternativa:
            // first = null;
            // Altra alternativa:
            // deleteFirst();
            return true;
        }
        else {
            ListElem iterator = first;
            while (iterator.getNext().getNext() != null) 
                iterator = iterator.getNext();
            iterator.setNext(null);
            return true;
        }
    }

    // I due metodi successivi sono un po' problematici per
    // via delle situazioni di errore che non possono essere
    // segnalate coerentemente con un valore di ritorno opportuno
    public int getFirstDato() {
        return first.getDato();
    }

    public int getLastDato() {
        ListElem iterator = first;
        while (iterator.getNext() != null)
            iterator = iterator.getNext();
        return iterator.getDato();
    }

    /* Esempi compiti */

    public ListaConcatenata reverseList() {
        ListaConcatenata reverse = new ListaConcatenata();
        if (first != null)
            reverse.first = first.reverseList(null);
        return reverse;
    }    

}
