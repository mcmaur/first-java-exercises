public class ListaString
{
	private class ListElem
	{
		private String dato;
		private ListElem next;

		public getDato()	
		{
			return this.dato;
		}

		public setDato(String newDato)
		{
			dato=newDato;
		}

		public ListElem getNext()
		{
			return this.next;
		}

		public void setNext(ListElem nxt)
		{
			this.next=nxt;
		}

		public boolean equals(String dt)
		{
			return this.dato.equals(dt);
		}

		public void toString()
		{
			System.out.println("[ " +dato+ " ]");
			if(next!=null)
				System.out.println("null");
			else
				System.out.println("next:");
		}
	private ListElem first;

	public ListaString() {
        first = null;
    }

	public void insert (String newDato)
	{
		ListElem elem = new ListElem ();
		elem.setDato(newDato);
		elem.setNext(first);
		first=elem;
	}

	public boolean delete (String daCercare)
	{
		boolean found;
		if(first!=null)
		{
			if (first.equals(daCercare))
			{
				first=first.getNext;
				found=true;
			}
			else
			{
				ListElem iterator = first;
				while(iterator.getNext() !=null && !found)
				{
					if(iterator.getNext().equals(daCercare))
					{
						found=true;
						iterator.setNext((iterator.getNext()).getNext());
					}
					else
					{
						iterator=iterator.getNext();
					}
				}
			}
		}
		else
			found=false;
		return found;
	}

	public void insertFirst(String val) {
        insert(val);
    }
	}
}
