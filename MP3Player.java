public class MP3Player {

    private SimpleID3[] list;

    public MP3Player() {
        list = null;
    }

    /*
    ** Il metodo seguente deve restituire true se la lista
    ** di SimpleID3 "list" e` ordinata secondo  la defini-
    ** zione data da comparteTo di SimpleID3
    **  
    */
	public boolean sorted() 
	{
		for (int i=0;i<list.length;i++)
		{
			if (list[i].compareTo(list[i+1])==1)
				return false;
		}
		return true;		
    }

    public void createList() {
        list = new SimpleID3[
            UtilLeggiTastiera.leggiInteroPositivoDaTastiera("Quanti elementi vuoi inserire?")
        ];
        for (int i = 0; i < list.length; i++)
            list[i] = new SimpleID3(
                UtilLeggiTastiera.leggiStringaDaTastiera("Artist: "),
                UtilLeggiTastiera.leggiStringaDaTastiera("Album: "),                 
                UtilLeggiTastiera.leggiInteroPositivoDaTastiera("Year: ")
             );       
    }

    public String toString() {
        String temp = "Contenuto del player:\n";
        temp +=       "---------------------\n";
        for (int i=0; i<list.length; i++)
            temp += list[i] + "\n"; 
        temp +=       "---------------------";
        return temp;
    }

}

