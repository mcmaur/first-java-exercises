import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

class MailingList
{
	//init argomenti e utenti, crea model e view	
	public static void main(String[] args) 
	{
		LinkedList<String> topics = new LinkedList<String>();
		topics.add("Science"); topics.add("Literature"); topics.add("Art"); topics.add("Sport"); topics.add("Music");
		LinkedList<String> users = new LinkedList<String>();
		users.add("Alice"); users.add("Bob"); users.add("Carl");
		MLModel mlModello = new MLModel(topics,users);
		MLView mlVista = new MLView(mlModello);
		//TO DO: add observer
		mlModello.addObserver(mlVista);
	}
}       


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//MODEL 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class MLModel extends Observable
{ 
	//TO DO implements|extends..
	LinkedList<String> topics;
	LinkedList<LinkedList<String>> liste; // per ogni argomento, lista di utenti abbonati
	LinkedList<String> users;
	LinkedList<LinkedList<String>> mailboxes; //per ogni utente, lista di messaggi ricevuti
	
	MLModel(LinkedList<String> topics, LinkedList<String> users)
	{
		this.users=users;
		mailboxes = new LinkedList<LinkedList<String>>();
		for (int i=0; i<users.size(); i++)
		{
		  mailboxes.add(new LinkedList<String>());
		}
		this.topics = topics;
		liste = new LinkedList<LinkedList<String>>();		
		for (int i=0; i<topics.size(); i++)
		{
   	  		liste.add(new LinkedList<String>());
		}
	}
	
	//cerca indice di elemento in lista
	int findIndex(LinkedList<String> list, String element)
	{
		//System.out.println("metodo findIndex iniziato");
		for (int i=0; i<list.size(); i++)
		{
			if(list.get(i).equalsIgnoreCase(element))
			{
				//System.out.println("index trovato:"+i);
				return i;
			}
		}
		return -1;
	}
	

	//iscrive (evitando duplicati)
	void subscribe(String user, String topic)
	{
		System.out.println("PROVA SUB: "+user+" disicritto dal topic: "+topic);
		int topicID =  findIndex(topics,topic);
        if(findIndex(liste.get(topicID),user)==-1)
        {
				liste.get(topicID).add(user);
				System.out.println("User: "+user+" al topic: "+topic);
			}
	}
	
	//disiscrive, se esiste
	void unsubscribe(String user, String topic)
	{
		System.out.println("TEST: "+user+" disicritto dal topic: "+topic);
		int topicID =  findIndex(topics,topic);
		int userID = findIndex(liste.get(topicID),user);
        if(userID!=-1)
        {
			liste.get(topicID).remove(userID);
			System.out.println("User: "+user+" disicritto dal topic: "+topic);
		}
	}
	
	//TO DO: add metodo che riceve messaggio e mette copia in mailboxes giuste
	public void send(String topic,String message)
	{
		/*findIndex(LinkedList<String> list, String element)*/
		LinkedList <String > listaUserPerTopic=liste.get(findIndex(topics, topic));
		for(int i=0;i<listaUserPerTopic.size();i++)
		{
			String tmp=listaUserPerTopic.get(i);
			mailboxes.get(findIndex(users,tmp)).add(message);
		}
		//System.out.println("send del modello");
		setChanged();
		notifyObservers();
	}
		
	//ritorna casella postale di un dato user
	LinkedList<String> getUserMail(String name){
		//System.out.println("metodo getUserMail iniziato");
		int userID = findIndex(users,name);
		//System.out.println("return del findIndex:"+userID);
		return mailboxes.get(userID);
	}

	
	//stampa a video snapshot modello
	void check(){		
		System.out.println("\nusers and their mailboxes: ");
		for (int i=0; i<users.size(); i++){
			 System.out.print(users.get(i)+": ");
			 for (int j=0; j<mailboxes.get(i).size(); j++) System.out.print(mailboxes.get(i).get(j)+" ");
			 System.out.println();				 
		}		
		System.out.println("\ntopics and subscriptions: ");
		for (int i=0; i<topics.size(); i++){
			 System.out.print(topics.get(i)+": ");
			 for (int j=0; j<liste.get(i).size(); j++) 
			 {
				System.out.print(liste.get(i).get(j)+" "); 
			 }
			 System.out.println();				 
		}
	}	
}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//VIEW-CONTROL
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
              

class MLView implements Observer
{  //TO DO: implements|extends..
	
	MLReaderView alice,bob,carl;
	
	MLView(MLModel ob)
	{
		MLWriterView zero = new MLWriterView("Newsletter Dispatcher",400,50,ob);
		alice = new MLReaderView("Alice",50,400,ob);  
		bob = new MLReaderView("Bob",450,400,ob); 
		carl = new MLReaderView("Carl",850,400,ob); 
	}
	
	//TO DO: metodo update, chiamerà updateMail()
	public void update(Observable obs,Object obj)
	{
		//System.out.println("update della view");
		alice.updateMail();
		bob.updateMail();
		carl.updateMail();
	}

}

class MLWriterView extends JFrame implements ActionListener
{
	MLModel model;
	String name;
    DefaultListModel mails;
	ButtonGroup topicGroup;
		
	MLWriterView(String name, int x, int y, MLModel ob){
		model = ob;
		JPanel p = new JPanel();
		setTitle(name); 
		JButton b = new JButton("Generate new mail");
		b.addActionListener(this);

		mails = new DefaultListModel();
        JList list = new JList( mails );
        JScrollPane listScrollPane = new JScrollPane(list); 
        listScrollPane.setPreferredSize(new Dimension(400, 200));

		JPanel interests = new JPanel();
		topicGroup = new ButtonGroup();
		JRadioButton radioButton;
		for (int i=0; i<model.topics.size(); i++) {
			if(i==0)
				radioButton = new JRadioButton(model.topics.get(i),true);
			else
				radioButton = new JRadioButton(model.topics.get(i));			
			radioButton.setActionCommand(model.topics.get(i));
			topicGroup.add(radioButton);
			interests.add(radioButton);
		}

		p.add(listScrollPane);
		p.add(interests); 
		p.add(b);
		add(p);
		setLocation(x,y);
		setVisible(true);  
		setPreferredSize(new Dimension(450,310));  
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
    //voglio generare mail su dato argomento
    public void actionPerformed(ActionEvent e){
		String topic = topicGroup.getSelection().getActionCommand();
		Random rnd = new Random();
		String message = " #"+rnd.nextInt(1000)+": a Quite Interesting fact about "+topic;
        mails.addElement(message); //nb: qui non c'è invio, solo display su finestra!
        //TO DO: call metodo send
        model.send(topic,message);
        }

} 


class MLReaderView extends JFrame implements ActionListener
{/*TO DO: implements..*/
	 
	MLModel model;
	String name;
    DefaultListModel mails;	
	JTextArea inboxBuffer;
	//TO DO: declare check boxes
	//~ JCheckBox Science,Literature,Art,Sport,Music;
	JCheckBox arCheck []; 
	JButton subscribe;
	
	MLReaderView(String name, int x, int y, MLModel ob){
		this.name = name;
		model = ob;
		JPanel p = new JPanel();
		setTitle(name); 
		// TO DO: layout del reader, come da classe esempio
		arCheck=new JCheckBox[5]; //numero topics da modello
		for(int i=0;i<arCheck.length;i++)
		{
			arCheck[i]= new JCheckBox(model.topics.get(i));
			p.add(arCheck[i]);
		}
		//~ Science=new JCheckBox("Science");
		//~ Literature=new JCheckBox("Literature");
		//~ Art=new JCheckBox("Art");
		//~ Sport=new JCheckBox("Sport");
		//~ Music=new JCheckBox("Music");
		subscribe= new JButton("Subscribe");
		subscribe.addActionListener(this);
		
		inboxBuffer = new JTextArea();
		inboxBuffer.setPreferredSize(new Dimension(300, 80));


		p.add(inboxBuffer);
		//TO DO: p.add(altri componenti)
		//~ p.add(Science);p.add(Literature);p.add(Art);p.add(Sport);p.add(Sport);p.add(Music);
		p.add(subscribe);
		
		
		add(p);
		setLocation(x,y);
		setVisible(true);  
		setPreferredSize(new Dimension(350,180));  
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	//TO DO: add metodi actionperformed, updateMail
	public void actionPerformed(ActionEvent e)
	{
		for(int i=0;i<arCheck.length;i++)
		{
			if(arCheck[i].isSelected())
				model.subscribe(name,model.topics.get(i));
			else
			model.unsubscribe(name,model.topics.get(i));
		}
	}
		
	public void updateMail()
	{
		System.out.println("\n");
		model.check();
		System.out.println("\n");
		LinkedList <String> list = model.getUserMail(name);
		String temp="";
		if(list.size()!=0)
			inboxBuffer.setText(list.getLast());
	}
	  
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
