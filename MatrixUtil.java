/*Esercizi opzionali: gestione di matrici (array bidimensionali)

Sviluppare la classe MatrixUtil.java, per la gestione di matrici, che contiene i metodi statici elencati sotto.
Testare i metodi in una classe ProvaMatrici.java che contiene solo il metodo main().*/
import java.util.*;
public class MatrixUtil
{

	/* public static int[] creaMatrice()
	- chiede all'utente il numero di righe r e di colonne c della matrice
	- crea una matrice di int con r righe e c colonne
	- inizializza la matrice prendendo valori da tastiera
	- restituisce la matrice creata
	*/
	public static int[][] creaMatrice() 
	{		
			Scanner tastiera =new Scanner (System.in);
			int c,r;
			System.out.print("Numero di righe? ");
			r=tastiera.nextInt();
			System.out.println();
			System.out.print("Numero colonne?");
			c=tastiera.nextInt();
			int [][] matrice = new int [r] [c];
	}
	
	
	/* public static void println(int[][] matr)
	visualizza a video il contenuto di una matrice di int
	*/
	public static int[][] println(int[][] matr) {
	/* sviluppare il codice del metodo */
	}
	
	
	/* public static int[][] somma(int[][] matr1, int[][] matr2)
	riceve due matrici di int di uguali dimensioni e restituisce la matrice somma
	*/
	public static int[][] somma(int[][] matr1, int[][] matr2) {
	/* sviluppare il codice del metodo */
	}
}	
