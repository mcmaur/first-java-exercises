/**
    legge da tastiera una sequenza di interi
    terminata da una stringa che non sia un intero
    e ne visualizza a video il massimo;
    se la sequenza � vuota visualizza il minimo valore di int
*/

import java.util.Scanner;

public class Max{

  public static void main(String[] args) {

	Scanner input = new Scanner(System.in);

    int max = Integer.MIN_VALUE; /*Variabile per contenere il massimo valore esaminato finora.
    								Viene inizializzata al piu' piccolo valore intero.
    								Cosi', qualunque numero sia inserito da
    								tastiera, sara' piu' grande di max. */
    int interoInput;/*Variabile per contenere il valore di input esaminato*/

    System.out.println("Inserisci una sequenza di interi terminata da LETTERA DELL'ALFABETO");

    while(input.hasNextInt()) { 	 /* Finch� il prossimo elemento da leggere nell'input e' un intero
    							        (cioe' finch� non incontra un non-intero) */
      interoInput = input.nextInt(); // Legge il prossimo intero

      if(interoInput > max) max = interoInput; /* Lo confronta con max. Se e' maggiore,
      											  aggiorna max */
    }

    System.out.println("il massimo e' " + max); //Visualizza il valore di max
  }// end main
}// end class
/* 	1) Cosa succede se non ci sono interi in input (per es., se vengono immessi solo non-interi) ?
	2) E se gli interi in input non vengono terminati da non-intero?
   	3) E se la sequenza in input e' vuota?
 */


