/*****************
Definire una classe MaxDiff il cui main:

- legge da tastiera una sequenza di numeri interi terminata da un non intero
- calcola la differenza fra il numero più grande e il numero più piccolo della sequenza (0 se la sequenza contiene solo un elemento)
- visualizza a video il risultato

Costringere l'utente a inserire solo numeri interi, e ad inserire almeno un numero*/

import java.util.Scanner;
public class MaxDiff
{
	public static void main (String[]args)
	{
	int input=0,max=0,min=0;
	Scanner tastiera = new Scanner(System.in);
	while (!tastiera.hasNextInt())
		{
		System.out.println("ERRORE:inserire num intero");
		tastiera.next();
		}
	max=tastiera.nextInt();
	min=max;
	while (tastiera.hasNextInt())
		{
		input=tastiera.nextInt();
		if (input>max)
			max= input;
		if (input<min)
			min=input;
		}
		System.out.println("La differenza tra : " + max +" e " + min +" = " + (max-min));
	}
}
