/**

    Legge da tastiera una sequenza di numeri interi
    terminata da una stringa che non sia un intero.
    Poi, visualizza a video il massimo numero inserito da tastiera.

    NB: se l'utente non scrive almeno un intero
    il programma continua a ripresentare la richiesta di input;
    questa versione � perci� migliore della precedente perche'
    costringe l'utente a dare l'input che l'applicazione si aspetta
    di ricevere.
*/

import java.util.Scanner;

public class MaxNew {

  public static void main(String[] args) {

    Scanner input = new Scanner(System.in);
    System.out.println("Immettere una sequenza di interi di almeno un elemento " +
    				   "terminata da una parola o da un non-intero:");

    while(!input.hasNextInt()) { // mentre l'utente digita caratteri che non
    					// corrispondono a numeri interi, svuota il buffer
    					// (senza salvare l'input perche' va ignorato)
      input.next();
      System.out.println("immetti almeno un intero"); // segnala l'errore a video
    }

    int x, max = input.nextInt(); // inizializza x e max con il primo numero digitato

    while(input.hasNextInt()) {
      x = input.nextInt();		// legge un nuovo numero da tastiera
      if(x > max) max = x;		// se il nuovo numero e' piu' grande del max, aggiorna max
    }
    System.out.println("Il massimo e' " + max);
  }
}

/*
Che cosa succede se non viene inserito nessun non-intero?
*/

