/*****************
Definire una classe MaxSequenzaNegativi il cui main:
- legge da tastiera una sequenza di numeri interi costituita da almeno un elemento e terminata da un non intero
- calcola la lunghezza della piu' lunga sequenza di valori negativi consecutivi
- visualizza a video il risultato

Esempio: dati la seguente serie di interi:
23, -2, 45, ***[ -34, -9, -4 ]***, 9, -8, -3
       
il programma visualizza 3, ossia la lunghezza della sequenza -34, -9, -4.

HELP: si suggerisce di mantenere le seguenti variabili:
- input: in cui si salvano via via i numeri letti da tastiera
- lungh: variabile che mantiene la lunghezza della sottosequenza di negativi corrente. Ogni volta che si trova il primo negativo di una sottosequenza, lungh deve essere inizializzata a 1
- maxLungh: variabile che tiene la lunghezza della piu' lunga sottosequenza di negativi incontrata durante l'esecuzione.*/

import java.util.Scanner;
public class MaxSequenzaNegativi
{
	public static void main (String[]args)
	{
	int lungh=0,maxlungh=0,input=0;
	Scanner tastiera = new Scanner(System.in);
	while (tastiera.hasNextInt())
	{
	input=tastiera.nextInt();
	if (input<0)
		lungh ++;
	else
		if (lungh>maxlungh)
			{
			maxlungh=lungh;
			lungh=0;
			}
	}
	if (lungh>maxlungh)
			{
			maxlungh=lungh;
			lungh=0;
			}
	System.out.println("La massima sequenza negativa è costituita da : "+maxlungh+" numeri");	
	}
}
