/****************
Definire una classe Media il cui main:

- legge da tastiera una sequenza di interi terminata da un non intero
- ne calcola la media aritmetica (con virgola)
- visualizza a video il risultato

Che cosa succede se la sequenza non contiene interi? Perché? Si risolva il problema costringendo l'utente ad inserire solo numeri interi (come in MaxNew.java) e ad inserire almeno un numero*/

import java.util.Scanner;
public class Media
{
	public static void main (String[]args)
	{
	int input=0,tot=0;
	float i=0,risultato=0;
	Scanner tastiera = new Scanner(System.in);
	while (tastiera.hasNextInt())
	{
	input=tastiera.nextInt();
	i++;
	tot=tot+input;
	}
	risultato= tot/i;	
	System.out.println("La media equivale a: " + risultato);
	}
}
