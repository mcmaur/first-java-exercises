/*ESECUTORE DI OPERAZIONI CON MENU CICLICO

Reimplementare l'esecutore di operazioni gia' sviluppato (somma, moltiplicazione, divisione, etc.) con menu ciclico. In dettaglio:

Sviluppare un'applicazione java il cui main:

1 - legge 2 numeri interi da tastiera
2 - chiede all'utente quale operazione vuole eseguire su tali numeri. Le operazioni tra cui scegliere sono la somma, la sottrazione, la moltiplicazione, la divisione (con virgola) e la media. Si consiglia di far specificare l'operazione come numero intero. Per es: 1 per la somma, 2 per la sottrazione, etc.etc., 0 per terminare il programma.

3a - Se l'utente ha digitato un numero associato ad un'operazione, l'applicazione visualizza a video il nome dell'operazione ed il valore degli argomenti; poi effettua l'operazione scelta e visualizza a video il risultato. (E rientra in ciclo)
3b - Se l'utente ha digitato un numero non previsto, visualizza un messaggio d'errore. (E rientra in ciclo)
3c - Se l'utente ha digitato '0' esce dal ciclo e termina l'esecuzione.*/

import java.util.Scanner;	
public class MenuCiclico
{
	public static void main (String[]args)
	{
	int x=0,y=0,scelta=40;
	double result=0;
	Scanner tastiera = new Scanner(System.in);
	System.out.println("Inserisci i due valori da calcolare");	
	System.out.print("Il primo: ");
	x=tastiera.nextInt();
	System.out.print("ed il secondo: ");
	y=tastiera.nextInt();	
	while (scelta!=0)
	{
	System.out.println();
	System.out.println("Decidi l'operazione da eseguire: 1-somma,2-sottrazione,3-moltiplicazione,4-divisione,5-media,0-uscita");
	scelta=tastiera.nextInt();	
		if (scelta==1)
		{
			System.out.print("L'operazione scelta è la somma di " +x+" e "+y+"il cui risultato è: ");
			result = x+y;
		}
		else
			if (scelta ==2)
			{
				System.out.print("L'operazione scelta è la sottrazione di " +x+" e "+y+"il cui risultato è: ");
				result = x-y;
			}
			else
				if (scelta ==3)
				{
					System.out.print("L'operazione scelta è la moltiplicazione di " +x+" e "+y+"il cui risultato è: ");
					result=x*y;
				}
				else
					if (scelta == 4)
					{
						System.out.print("L'operazione scelta è la divisione di " +x+" e "+y+"il cui risultato è: ");
						result = (double)x / y;
					}
					else
						if (scelta==5)
						{
							System.out.print("L'operazione scelta è la media di " +x+" e "+y+"il cui risultato è: ");
							result= (x+y)/2.0;
						}
			if (scelta!=0 && scelta!=1 && scelta!=2 && scelta!=3 && scelta!=4 && scelta!=5)
			{
				System.out.println("ERROR OF INPUT");
				break;
			}
		System.out.print(+ result);
		}
	}
}	


