public class MergeSortConti
{
	private static ContoCorrente [] Vettore;

	public static void split(ContoCorrente [] array)
	{
		for(int i=0;i<array.length;i++)
		Vettore[i]=array[i];
		if(array.length>1)
		{
			int size1=array.length/2;
			int size2=array.length - size1;
			ContoCorrente [] firstHalf=new ContoCorrente [size1];
			ContoCorrente [] secondHalf=new ContoCorrente [size2];
			for(int i=0;i<size1;i++)
				firstHalf[i]=array[i];
			for(int i=0;i<size2;i++)
				secondHalf[i]=array[size1+i];
			split(firstHalf);
			split(secondHalf);
			merge (firstHalf,secondHalf);
		}
	}

	private static ContoCorrente [] merge (ContoCorrente [] one, ContoCorrente [] two)
	{
		int i=0,j=0,k=0;
		ContoCorrente [] Final = new ContoCorrente [Vettore.length];
		while (i<one.length && j<two.length)
		{
			if(one[i].GetNumConto()<two[j].GetNumConto())
			{
				Final[k]=one[i];
				i++;
				k++;
			}
			else
			{
				Final[k]=two[j];
				j++;
				k++;
			}
		}	
		while(i>=one.length && j<two.length)
		{
			Final[k]=two[j];
			j++;
		}
		while(j>=two.length && i<one.length)
		{
			Final[k]=one[i];
			i++;
		}
		return Final;
	}
}
