public class MioVett
{
	private int [] datiVett;
	private int lung;

	public MioVett (int dimens)
	{ 
		datiVett = new int [dimens];
		lung = dimens; 
	}

	public int elem (int pos)
	{
		return datiVett [pos]; 
	}
	public void store (int pos, int val)
	{ 
		datiVett [pos] = val; 
	}
	public void mergeSort1 ()
	{
		if (lung != 1)
		{
			int size1 = lung / 2;
			int size2 = lung - size1;
			MioVett firstHalf = new MioVett (size1);
			MioVett secondHalf = new MioVett (size2);
			for (int i = 0; i < size1; i++)
			firstHalf.store (i, datiVett [i]);
			for (int i = 0; i < size2; i++)
			secondHalf.store (i, datiVett [size1 + i]);
			firstHalf.mergeSort1 ();
			secondHalf.mergeSort1 ();
			this.merge1 (firstHalf, secondHalf);
		}
	}
	private void merge1 (MioVett vett1, MioVett vett2)
	{
		int i1 = 0;
		int i2 = 0;
		for (int iOut = 0; iOut < vett1.lung + vett2.lung; iOut++)
		{
			if (i1 == vett1.lung)
				datiVett [iOut] = vett2.elem (i2++);
			else 
			{
				if (i2 == vett2.lung)
					datiVett [iOut] = vett1.elem (i1++);
				else 
				{
					if (vett1.elem (i1) > vett2.elem (i2))
						datiVett [iOut] = vett2.elem (i2++);
					else 
						datiVett [iOut] = vett1.elem (i1++);
				} // End of 'if vett1'
			} // End of 'if i2'
		} // End of 'if i1'
	} // End of Merge !
}

