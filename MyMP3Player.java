public class MyMP3Player {

    public static void main(String[] args) {
   
        MP3Player ipod = new MP3Player();

        /*
        ** Creare una lista, 
        */
        ipod.createList();
  
        /*
        ** Stampare la lista, 
        */
        System.out.println(ipod);

        /*
        ** Verificare se la lista e` ordinata e stampare
        ** il risultato
        */
        System.out.println("Ordinata: " + ipod.sorted());
 
    }

}

