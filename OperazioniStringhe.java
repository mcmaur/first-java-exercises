/**
* La classe OperazioniConStringhe ricapitola alcune operazioni che
* si possono applicare sugli oggetti Stringa.
* Scopo dell'esercizio, e' usare alcuni fra i metodi piu' importanti della
* classe String.
**
* Per completare l'esercizio, sviluppare il codice ove indicato da "TO-DO"
*/

import java.util.Scanner;

class OperazioniStringhe {

	public static void main(String args[]) {

		Scanner tastiera = new Scanner(System.in);

		String mionome;
// TO DO: chiedere all'utente il suo nome (da tastiera) e salvarlo in "mionome"
		System.out.println("Inserisci il tuo nome");		
		mionome=tastiera.nextLine();

// Visualizza a video il valore di "mionome" cosi' com'e', tutto maiuscolo e tutto minuscolo.Per ottenere la stringa tutta maiuscola/minuscola si usano ii metodo String toUpperCase(),String toLowerCase() di String.
		System.out.println("Il mio nome e': " + mionome + " (" + mionome.toUpperCase() + "," +
							mionome.toLowerCase() + ")");


		String[] nomi = new String[5]; // nomi: array di String con 5 elementi
// TO-DO: inizializzare "nomi" con nomi acquisiti da tastiera.a tale scopo usare un ciclo FOR in cui si chiede all'utente di digitare i nomi.
		for (int i=0;i<6;i++)
			nomi [i]=tastiera.nextLine();

// TO-DO: Visualizzare a video i nomi salvati in "nomi", con ciclo FOR
		for (int i=0;i<6;i++)
			System.out.prinln(nomi[i]);

// TO-DO: per ogni nome nell'array, controllare l'ordine lessicograficorispetto a mionome e visualizzare a video il risultato
		for (int i = 0; i < TO-DO; i++) {
			System.out.println();
			System.out.println("\tAnalisi di '"+nomi[i]+"'");
		int c = mionome.compareTo(nomi[i]); // usare il metodo 'compareTo()' per comparare "mionome" con "nomi[i]"
			if (c>0) {
				System.out.println("Ordinamento lessicografico: "+mionome+"" + " > " + nomi[i]);
			}
			else if (c=0) {
				System.out.println("Ordinamento lessicografico: "+mionome+"" + " uguale a " + nomi[i]);
				}
				else
					System.out.println("Ordinamento lessicografico: "+mionome+"" + " < " + nomi[i]);
		}


// TO-DO: per ogni nome in "nomi", calcolare la lunghezza del nome e visualizzare a video il risultato
		for (int i = 0; i < TO-DO; i++) {
			int lungNome = nomi[i].length(); // assegnare a lungNome la lunghezza di nomi[i]a tale scopo, usare il metodo public int length() di String
			System.out.println("'"+nomi[i]+"'" + " ha lunghezza: " + lungNome);
		}


/* TO-DO: per ogni nome nell'array, verificare se il nome e' una sottostringa di "mionome" e visualizzare a video il risultato.per sapere se una stringa e' sottostringa di un'altra, usare il metodo"public int indexOf(String x)" di String che:- se "mionome" contiene "x", restituisce l'indice di inizio di "x" in "mionome" - altrimenti, restituisce -1*/

		for (int i = 0; i < TO-DO; i++) {
			if (mionome.indexOf(nomi[])!= -1)
				System.out.println("'"+nomi[i]+"'" + " è una sottostringa di "+ mionome);
		}


// TO-DO: per ogni nome nell'array, estrarre sotto-stringhe e le manipolarle perottenere altre stringhe.usare substring() e concat(): tagliare la stringa a metà,prendere le due singole metà e concatenarle in ordine inverso
		for (int i = 0; i < TO-DO; i++) {
			String s1 = nomi[i].substring(0,nomi[i].lenght/2);
			String s2 = nomi[i].substring(nomi[i].lenght/2);
			String s = s2+s1;
			System.out.println("Stringa manipolata: " + s);
		}

// TO-DO: per ogni nome nell'array, controllare se e' una stringapalindroma o meno. NB: una stringa è palindroma quando, scritta al contrario rimane identica a se stessa. Es: anna e' palindroma; abcd non e' palindroma.
		for (int i = 0; i < nomi[].length; i++) {
			int l =1;
			do
			String inversa = +parola.charAt(nomi[i].length-l)+;
			while (l<nomi[i].length)
// per creare la stringa inversa, usare il metodo char charAt(int index) di String che restituisce il carattere con indice index e concatenare i caratteri in ordine inverso

			if (inversa.equalsIgnoreCase(nomi[i]))
				System.out.println("La parola '" + nomi[i] + "' e' palindroma.");
			else
				System.out.println("La parola '" + nomi[i] + "' non e' palindroma.");

		} // end CICLO FOR su array "nomi"

	} // end main

} // end OperazioniConStringhe
