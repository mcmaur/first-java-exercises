import java.util.Scanner;
public class Palindromi
{
	public static void main (String[]args)
	{
		boolean flag=true;
		int i=0, y=0;
		Scanner tastiera = new Scanner(System.in);
		System.out.print("Insersci nome da analizzare: ");		
		String parola = tastiera.nextLine();
		System.out.println();
		for (i=0,y=parola.length()-1;i<parola.length() && y>0 && flag;i++,y--)
		{
			if (parola.charAt(i)!=parola.charAt(y))
				flag=false;
				break;
		}
		if (flag==false)
			System.out.println(parola +" e' non palindromo");
		else
			System.out.println(parola +" e' palindromo");
	}
}
