/** Classe Persona: descrive persone che abitano in una citta'.
 */

public class Persona {

	private String nome;
	private String cognome;
	private String citta;
	private String nazionalita;


 /** Costruttore di Default vuoto */
  public Persona() {

		nome = " ";
		cognome =" ";
		citta =" ";
		nazionalita = " ";
  }

 /** Costruttore con parametri*/
  public Persona(String nomeP,String cognomeP, String cittaP, String nazP) {
		nome = nomeP;
		cognome = cognomeP;
		citta = cittaP;
		nazionalita = nazP;
  }

  public String getNome () {
		return nome;
  }

  public String getCognome() {
	  return cognome;
  }

  public String getCitta() {
	  return citta;
  }

  public String getNazionalita() {
	  return nazionalita;
  }

  public String toString() {
	  return nome + " " + cognome
		     + " citta' di residenza : " + citta
     		 + " nazionalita' : " + nazionalita;
  }


    // PER L'ESAME: Definire il metodo specificato nel seguito

    /** il metodo

    		public boolean risiede(String[] citta)

    	verifica se this risiede in una delle localita' memorizzate
    	nell'array citta. In caso positivo, il metodo restituisce true; false altrimenti.

    	Per esempio, si supponga che l'array citta contenga
    	i seguenti nomi di citta: Roma, Torino, Milano.
    	Se this risiede a Torino, il metodo restituisce true.
    	Se invece this risiede a Genova, il metodo restituisce false.
    */


    	public boolean risiede(String[] citta) {

    		/* sviluppare il codice del metodo */
    	}


}// fine definizione classe

