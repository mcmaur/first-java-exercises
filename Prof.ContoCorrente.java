/*

 Classe che implementa una struttura dati che rappresenta un conto corrente

 *** IN FASE DI SVILUPPO

*/

public class ContoCorrente {


  private String nome; // Nome del proprietario
  private double saldo; // Salto del conto, deve essere >= 0
  private String iban;   // Numero del conto
  private static int contatore; // Numero di conti creati
  static final String IBAN_PREFIX = "iban-";



  /**
  Crea un conto corrente
  */

  public ContoCorrente(String ilNome, double primoVersamento) {
	  nome = ilNome;
	  saldo = primoVersamento;
	  iban = IBAN_PREFIX + contatore;
	  contatore++;
  }


  /**
    Crea un conto corrente con saldo zero
  */

  public ContoCorrente(String ilNome) {
	  this(ilNome, 0);
  }



  /**
     Restituisce il nome del proprietario
  */
  public String getNome() {
	  return nome;
  }



 /**
     Imposta un nuovo nome per il proprietario
  */
  public void setNome(String ilNome) {
	  nome = ilNome;
  }



  /**
       Restituisce il saldo
  */
   public double getSaldo() {
  	  return saldo;
  }


  /**
         Restituisce il numero del conto
  */
  public String getIban() {
        return iban;
  }


  /**
       Effettua un versamento
       @param importo l'importo da versare, deve essere >= 0
  */
  public void versamento(double importo) {
	      if (importo < 0) throw new IllegalArgumentException();

      	  saldo += importo;
  }


   /**
        Effettua un prelievo
   */
   public void prelievo(double prelievo) throws NoSuchMethodException {
        	  throw new NoSuchMethodException();
  }

   /**
          Restituisce una rappresentazione testuale del conto corrente
   */
   public String toString()  {
       String s = ""; // Accumula il risultato

       s += "Correntista: " + nome + "\n";
       s += "IBAN: " + iban + "\n";
       s += "Saldo: " + saldo + "\n";

       return s;

  }



}