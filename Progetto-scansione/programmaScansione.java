import java.io.*;

public class programmaScansione {
    private static String[] extensions = { "*.jpg", "*.tiff", "*.jpeg", "*.png", "*.bmp", "*.gif", "*.psd" };
    private static File userHome = new File(System.getProperty("user.home"));
    private static int contaNumFile = 0;
    private static long weightFile = 0;

    private static class SearchReport extends PrintStream
    {
        public SearchReport(OutputStream os)
        {
            super(os);
        }

        public void report(File f)
        {
            println("Found " + f.getPath());
            println("Size: " + (f.length() / 1024) + " Kilobytes");
            println("Hidden: " + f.isHidden());
            println();
        }
    }

    private static class FileName
    {
        protected final String name;
        protected final String extension;

        public FileName(String fileName)
        {
            int lastDot = fileName.lastIndexOf('.');

            if (lastDot == -1)
            {
                name = fileName;
                extension = "";
            }
            else
            {
                name = fileName.substring(0, lastDot);
                extension = fileName.substring(lastDot + 1);
            }
        }

        public String getName()
        {
            return name;
        }

        public String getExtension()
        {
            return extension;
        }

        public boolean matchesName(FileName other)
        {
            boolean otherIsWildcard = other.name.equals("*");
            boolean sameName = name.equalsIgnoreCase(other.name);
            return (otherIsWildcard || sameName);
        }

        public boolean matchesExtension(FileName other)
        {
            return extension.equalsIgnoreCase(other.extension);
        }

        public boolean matches(FileName other)
        {
            return (matchesName(other) && matchesExtension(other));
        }
    }

    public static void findAllFiles(File directory, String pattern, SearchReport out) {
        FileName targetName = new FileName(pattern);
        try {
            for (File f : directory.listFiles())
            {
                if (f.isDirectory())
                {
                    findAllFiles(f, pattern, out);
                }
                else if (f.isFile())
                {
                    FileName fileName = new FileName(f.getName());
                    if (fileName.matches(targetName))
                    {
                        out.report(f);
                        weightFile += f.length();
                        ++contaNumFile;
                    }
                }
            }
        } catch (NullPointerException ex) {
            // It might be thrown when accessing the result of
            // directory.listFiles() in the for loop, as it returns null when
            // the object does not represent a directory or when an I/O error
            // occurs, like when we don't have enough permission to have
            // access to the specified filesystem entry.
        } catch (Exception ex) {
            System.out.println("Error: " + ex.toString());
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try
        {
            SearchReport out = new SearchReport(new FileOutputStream("Report.txt"));
            for (String ext : extensions)
            {
                findAllFiles(userHome, ext, out);
            }
            out.println();
            out.println("For a total of: " + (weightFile / 1024) + " Kilobytes or " + (weightFile / 2048) + " Megabytes or " + (weightFile / 4096) + " Gb");
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("Error: " + ex.toString());
        }
    }
}