import java.util.*;

public class ProvaDiEsame01 {

	public static void main(String[] args) {

		Dipendente[] dipendenti = new Dipendente[3];
		dipendenti[0] = new Dipendente("M1234", 1500.00);
		dipendenti[1] = new Dipendente("M5678", 2500.00);
		dipendenti[2] = new Dipendente("M9999", 3500.00);

		for (int i=0; i<dipendenti.length; i++)
			System.out.println(dipendenti[i]);
		System.out.println();


//      L'array di double, "fasceStipendio", deve essere riempito con
//		i valori (in ordine crescente) di
//		3 fasce stipendiali di riferimento (fascia 0, fascia 1, fascia 2).
		double[] fasceStipendio = ArrayUtil.readDoubleArray();


		System.out.println("Scegli la fascia stipendiale di riferimento: ");
		Scanner sc = new Scanner(System.in);
		int fascia = sc.nextInt();
		int num = quantiDipendenti(dipendenti, fascia, fasceStipendio);
		System.out.println("Ci sono " + num + " dipendenti in fascia stipendiale " + fascia);
	} // fine main



// (1) Definire il metodo
//			public static int quantiDipendenti (Dipendente[] dip, int f, double[] fasceStip)
//		 descritto nel seguito (e il metodo getFascia(Dipendente d, double[] fasceStip)
//		 utilizzaro da quantiDipendenti().




/** Il metodo

		public static int quantiDipendenti (Dipendente[] dip, int f, double[] fasceStip)

	restituisce il numero di dipendenti che, date le fascie stipendiali fasceStip,
	appartengono alla fascia f

	Per valutare la fascia di appartenenza di un determinato dipendente,
	quantiDipendenti() utilizza il metodo
		public static int getFascia(Dipendente d, double[] fasceStip)

*/

	public static int quantiDipendenti (Dipendente[] dip, int f, double[] fasceStip) 
	{
		int n=0;
		for (int i=0;i<dip.length;i++)
		{
			if(getFascia(dip[i],fasceStip)==f)
				n++;
		}
		return n; 
	}


/** il metodo

		public static int getFascia(Dipendente d, double[] fasceStip)

	restituisce la fascia stipendiale del dipendente d:
	fascia 0 se lo stipendio e' inferiore o uguale al valore di riferimento in fasceStip[0];
	fascia 1 se e' superiore a fasceStip[0] ma inferiore o uguale a fasceStip[1];
	fascia n se e' superiore a fasceStip[n-1], ultima fascia stipendiale definita.
*/

	public static int getFascia(Dipendente d, double[] fasceStip) 
	{
		int n=0;
		if(d.getStipendio()<=fasceStip[0])
			n=0;
		else
		{
			if(d.getStipendio()<=fasceStip[1])
				n=1;
			else
			{
				if(d.getStipendio()<=fasceStip[2])
					n=2;
			}
				
		}
		return n;			
	}
}
