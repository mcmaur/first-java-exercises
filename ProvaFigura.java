public class ProvaFigura
{
	public static void main (String[]args)
	{
		Figura [] arrayFigure = new Figura [4];
		arrayFigure[0]=new Cerchio(4);
		arrayFigure[1]=new Rettangolo(4,3);
		arrayFigure[2]=new Quadrato(4);
		arrayFigure[3]=new TriangoloRettangolo(4,3);

		for(Figura f : arrayFigure)
			System.out.println(f);
	}
}
