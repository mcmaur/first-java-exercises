import java.util.*;


public class ProvaStringArrayOrd{


	public static void main(String[] args) {

		StringArrayOrd ar = new StringArrayOrd(5);
		ar.insOrd("Giorgio");
		ar.insOrd("Filippo");
		ar.insOrd("Maria");
		ar.insOrd("Anna");
		ar.insOrd("Maria");
		ar.insOrd("Roberto");
		ar.insOrd("Assunta");
		ar.insOrd("Giovanni");
		ar.insOrd("Ugo");
		ar.insOrd("Rachele");
		ar.insOrd("Gigi");
		ar.print();
		System.out.println("POI...");
		ar.cancella("Maria");
		ar.cancella("Filippo");
		ar.cancella("Anna");
		ar.cancella("Filippo");
		ar.cancella("Giorgio");
		ar.cancella("Maria");
		ar.print();
	}



}
