import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


class PuzzleFrame extends JFrame
{
	private int size;
	private PuzzleButton emptyButton;//lo 'spazio vuoto'

	public PuzzleFrame (int size)
	{
		int valore=0;
		setLayout(new GridLayout(size, size));
//4_TO DO: inserire bottoni in griglia
		for(int i=0;i<size;i++)
		{
			for(int j=0;j<size;j++)
			{
				if(i==j && i==(size-1))
				{
					emptyButton=new PuzzleButton(i,j,0);
					add(emptyButton);
				}
				else
				{
					valore++;
					add(new PuzzleButton(i,j,valore));
				}
			}
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("PuzzleFrame");
		setSize(size * 50 + 10,  size * 50 + 20);
		setVisible(true);
   }


//********************************


class PuzzleButton extends JButton implements ActionListener
{ //2a_TO DO: implementare un'interfaccia -listener  
	private int row, column, val;

	public PuzzleButton(int r, int c, int val){  
		row = r;
		column = c;
		val=val;
		
		if (val>0)
		{
			setBackground(Color.white);
       	setText("" + val);
     	}
     	else
     	{
			setBackground(Color.black);
			setText("");
     	}

        addActionListener(this);
	}


	private boolean adjacent(PuzzleButton b)
	{
//1_TO DO: return true if bottoni adiacenti
		if(this.row==b.row)
			if(this.column - b.column==1 || this.column - b.column==-1)
				return true;
		if(this.column==b.column)
			if(this.row - b.row==1 || this.row - b.row==-1)
				return true;
		return false;
	}


	public void actionPerformed(ActionEvent e)
	{  //2b_TO DO: inserire l'evento come parametro
//3_TO DO: controllare se la sorgente è adiacente all'empty button e modificare i bottoni di conseguenza
		if(this.adjacent(emptyButton))
		{
// 			PuzzleButton tempButton = emptyButton.clone;
	
			emptyButton.setText(this.getText());
			emptyButton.setBackground(Color.white);
			this.setBackground(Color.black);
			this.setText("");
			emptyButton=this;
		}
	}
	
}//end of class PuzzleButton


}//end of class PuzzleFrame


//********************************


class Puzzle
{	
	public static void main(String[] args)
	{ 
  		int size = 4;
    	PuzzleFrame frame = new PuzzleFrame(size);
	}
}//end of class Puzzle
