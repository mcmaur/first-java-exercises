import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.event.*;

class PuzzleFrame extends JFrame
{
	private int size,choice;
	private PuzzleButton emptyButton;
	private ArrayList <Integer> list;
	private JButton reset;
	private JPanel panelInfo,panelPuzzle;	
	private JMenu Mkind,Msize,Mabout;
	private JMenuItem menuItem;
	private javax.swing.Timer timer;
	private long startTime;
	private JLabel labelTime;	
	private JMenuBar menuBar;
	private boolean first_istance;
	private JLabel labelActualSize;
	private JSlider sliderSize;
	private JFrame frameSlider;
	
	/*constructor of puzzleFrame*/
	public PuzzleFrame (int b_size,int command)
	{
		size=b_size;
		choice = command;
		createMenu();
		createInfoPanel();
		createPuzzlePanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("PuzzleFrame");
		setSize(size * 70 + 10,size * 70 + 20);
		setLocationRelativeTo(null);
		setVisible(true);
   }

	/*"constructor" of the puzzle panel: main window (in puzzleFrame)*/
   private void createPuzzlePanel()
   {
		first_istance=true;
		panelPuzzle = new JPanel(new FlowLayout());
		panelPuzzle.setLayout(new GridLayout(size, size));
		initialize(choice);
		add(panelPuzzle,BorderLayout.CENTER);
	}

	/*"constructor" of the info bar (in puzzleFrame)*/
   private void createInfoPanel()
	{
		panelInfo = new JPanel(new FlowLayout());
		labelTime = new JLabel("00:00:00");
		panelInfo.add(labelTime);
		timer = new javax.swing.Timer (90, new ActionListener () 
			{
            public void actionPerformed (ActionEvent e)
            {
                long diffTime = System.currentTimeMillis () - startTime;
                int decSeconds = (int) (diffTime % 1000 / 100);
                int seconds = (int) (diffTime / 1000 % 60);
                int minutes = (int) (diffTime / 60000 % 60);
                int hours = (int) (diffTime / 3600000);
                String s = String.format ("%d:%02d:%02d.%d", hours, minutes,seconds, decSeconds);
                labelTime.setText (s);
            }
        });
		reset = new JButton("Reset");
		panelInfo.add(reset);
		reset.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				reset();
			}
		});
		add(panelInfo,BorderLayout.SOUTH);
		panelInfo.setSize(size * 50 + 10,50);
	}
   
   /*"constructor" of the menu (in puzzleFrame)*/
   private  void createMenu()
   {
		menuBar = new JMenuBar();
		
		Mkind = new JMenu("Kind");
		Mkind.setMnemonic(KeyEvent.VK_N);
		menuItem=new JMenuItem("Ordered"/*,KeyEvent.VK_T*/);
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					choice=1;
					reset();
				}
			});
		Mkind.add(menuItem);
		menuItem=new JMenuItem("Reversed");
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					choice=2;
					reset();
				}
			});
		Mkind.add(menuItem);
		menuItem=new JMenuItem("Random");
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					choice=3;
					reset();
				}
			});
		Mkind.add(menuItem);
		menuItem=new JMenuItem("Rotate Left");
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					choice=4;
					reset();
				}
			});
		Mkind.add(menuItem);
		menuBar.add(Mkind);
		
		Msize = new JMenu("Size");
		menuItem=new JMenuItem(" 3 ");
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					size=3;
					reset();
				}
			});
		Msize.add(menuItem);
		menuItem=new JMenuItem(" 4 ");
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					size=4;
					reset();
				}
			});
		Msize.add(menuItem);
		menuItem=new JMenuItem(" 5 ");
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					size=5;
					reset();
				}
			});
		Msize.add(menuItem);
		menuItem=new JMenuItem(" 6 ");
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					size=6;
					reset();
				}
			});
		Msize.add(menuItem);
		menuItem=new JMenuItem(" Other... ");
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					frameSlider = new JFrame();
					JPanel panelSlider = new JPanel();
					JLabel labelSize = new JLabel ("Set size");
					panelSlider.add(labelSize);
					sliderSize= new JSlider(7, 10, 8);
					sliderSize.setVisible(true);
					labelActualSize = new JLabel ("8");
					labelActualSize.setVisible(true);
					panelSlider.add(sliderSize);
					panelSlider.add(labelActualSize);
					sliderSize.addChangeListener(new ChangeListener()
						{
							public void stateChanged (ChangeEvent e)
							{
								labelActualSize.setText(Integer.toString(sliderSize.getValue()));
							}
						});
					JButton submit = new JButton("OK");
					submit.addActionListener(new ActionListener()
						{
							public void actionPerformed(ActionEvent e)
							{
								size=sliderSize.getValue();
								reset();
								frameSlider.dispose();
							}
						});
					panelSlider.add(submit);
					frameSlider.setTitle("Size");
					frameSlider.setSize(300,80);
					frameSlider.setLocationRelativeTo(null);
					frameSlider.add(panelSlider);
					panelSlider.setVisible(true);
					frameSlider.setVisible(true);
				}
			});
		Msize.add(menuItem);
		menuBar.add(Msize);
		
		Mabout = new JMenu("About");
		menuItem=new JMenuItem("Credits");
		Mabout.add(menuItem);
		menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					JFrame frameCredits = new JFrame();
					JPanel panelCredits = new JPanel();
					frameCredits.add(panelCredits);
					JLabel labelCredits = new JLabel ("AUTORE: Mauro Cerbai");
					panelCredits.add(labelCredits);
					JLabel labelCredits2 = new JLabel ("con la gentile collaborazione con:");
					panelCredits.add(labelCredits2);
					JLabel labelCredits3 = new JLabel ("-Gianluca Bortignon");
					panelCredits.add(labelCredits3);
					JLabel labelCredits4 = new JLabel ("-Andrea Costa");
					panelCredits.add(labelCredits4);
					frameCredits.setTitle("CREDITS");
					frameCredits.setSize(250,130);
					frameCredits.setLocationRelativeTo(null);
					frameCredits.setVisible(true);
					//~ frameCredits.pack();					
				}
			});
		Mabout.add(menuItem);
		menuBar.add(Mabout);
		
		this.setJMenuBar(menuBar);
	}

	private void reset ()
	{
		timer.stop ();
		remove(panelPuzzle);
		repaint();
		createPuzzlePanel();
		repaint();
		setSize(size * 70 + 10,size * 70 + 20);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	/*fill the gridlayout because of choice 1:ordered;2:reversed;3:random (in puzzleFrame)*/
	private void initialize(int command)
	{
		int valore;
		switch (command)
		{
			case 1:	//Ordered
			{
				valore=0;
				for(int i=0;i<size;i++)
				{
					for(int j=0;j<size;j++)
					{
						if(i==j && i==(size-1))
						{
							emptyButton=new PuzzleButton(i,j,0);
							panelPuzzle.add(emptyButton);
						}
						else
						{
							valore++;
							panelPuzzle.add(new PuzzleButton(i,j,valore));
						}
					}
				}
				break;
			}
			case 2: //Reversed
			{
				valore=size * size - 1;
				for(int i=0;i<size;i++)
				{
					for(int j=0;j<size;j++)
					{
						if(i==j && i==(size-1))
						{
							emptyButton=new PuzzleButton(i,j,0);
							panelPuzzle.add(emptyButton);
						}
						else
						{
							panelPuzzle.add(new PuzzleButton(j,i,valore));
							valore--;
						}
					}
				}
				break;
			}
			case 3: //Random
			{
				list = new ArrayList<Integer>(size*size-1);
				for(int i = 1; i <= (size*size-1); i++) 
				{
					list.add(i);
				}

				for(int i=0;i<size;i++)
				{
					for(int j=0;j<size;j++)
					{
						if(i==j && i==(size-1))
						{
							emptyButton=new PuzzleButton(i,j,0);
							panelPuzzle.add(emptyButton);
						}
						else
						{
							panelPuzzle.add(new PuzzleButton(i,j,shuffle()));
						}
					}
				}
				break;
			}
			case 4:
			{
				int i=0,j=0;
				valore=1;
				while(i<size)
				{
					if(j<size)
					{
						if (j==(size-1) && i==(size-1))
						{
							emptyButton = new PuzzleButton(i,j,0);
							panelPuzzle.add(emptyButton);
							j++;
						}
						else
						{
							panelPuzzle.add(new PuzzleButton(i,j,(valore+(size*j))));
							j++;
						}
					}
					else
					{
						j=0;
						valore++;
						i++;
					}
				}
				break;
			}
			default:
			{
				throw new IllegalArgumentException();
			}
		}
	}

	/*return casual int from 1 to (size*size)-1 deleting it from an array of integer in order to avoid the possibility to extract again*/
	private int shuffle ()
	{
        Random rand = new Random();
        while(list.size() > 0)
        {
            int index = rand.nextInt(list.size());
            return(list.remove(index));
        }
        return -1;
	}

//************************************************ CLASSE INTERNA PUZZLEBUTTON *********************************
class PuzzleButton extends JButton implements ActionListener
{
	private int row, column, val;

	public PuzzleButton(int r, int c, int val)
	{  
		row = r;
		column = c;
		val=val;
		
		if (val>0)
		{
			setBackground(Color.white);
			setText("" + val);
     	}
     	else
     	{
			setBackground(Color.gray);
			setText("");
     	}

        addActionListener(this);
	}


	private boolean adjacent(PuzzleButton b)
	{
		if(this.row==b.row)
			if(this.column - b.column==1 || this.column - b.column==-1)
				return true;
		if(this.column==b.column)
			if(this.row - b.row==1 || this.row - b.row==-1)
				return true;
		return false;
	}


	public void actionPerformed(ActionEvent e)
	{
		if(this.adjacent(emptyButton))
		{
			if(first_istance)
			{
				startTime = System.currentTimeMillis ();
				timer.start ();
				first_istance=false;
			}
			emptyButton.setText(this.getText());
			emptyButton.setBackground(Color.white);
			this.setBackground(Color.gray);
			this.setText("");
			emptyButton=this;
		}
	}
	
}//end of class PuzzleButton


}//end of class PuzzleFrame

//**************************************************  MAIN  ****************************************************

class Puzzle
{	
	public static void main(String[] args)
	{
		PuzzleFrame frame = new PuzzleFrame(4,3);
	}
}//end of class Puzzle
