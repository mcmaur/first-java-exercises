public class Quadrato extends Rettangolo 
{
	private double l;
	public Quadrato(double l)
	{
		super(l,l);
	}
	public String toString()
	{
		return ("Sono un quadrato. Le mie misure sono:\n area: "+area()+"\n perimetro: "+perimetro()+"\n num lati: "+numeroLati());
	}
}
