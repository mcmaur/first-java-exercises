public class Rettangolo implements poligono
{
	private double base,height;
	public Rettangolo(double b,double h)
	{
		base=b;
		height=h;
	}
	public double area()
	{
		return base*height;
	}
	public double perimetro()
	{
		return base*2+height*2;
	}
	public int numeroLati()
	{
		return 4;
	}
	public String toString()
	{
		return ("Sono un rettangolo. Le mie misure sono:\n area: "+area()+"\n  perimetro: "+perimetro()+"\n num lati: "+numeroLati());
	}
}
