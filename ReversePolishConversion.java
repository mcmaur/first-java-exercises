import java.lang.Integer;
public class ReversePolishConversion
{
	private static int [] stack;
	private static int StackPointer;

	public static void main (String[]args)
	{
		StackPointer=0;
		String exp = "32+4*";
		int [] stack=new int [1000];
		for(int i=0;i<exp.length();i++)
		{
			if(exp.charAt(i)=='+' || exp.charAt (i)=='*')
				pop(exp.charAt(i));
			else
			{
				push((Character.digit(exp.charAt(i),10)),StackPointer);
				StackPointer++;
			}
		}
		System.out.println("Finito");
		System.out.println(stack[0]);
	}

	private static void push (int i,int StackPointer)
	{
		stack [StackPointer]=i;
	}

	private static void pop(char c)
	{
		int first,second,result;
		first=stack[StackPointer];
		StackPointer--;
		second=stack[StackPointer];
		if(c=='+')
			result=first+second;
		else
			result=first*second;
		push(result,StackPointer);
	}
}
