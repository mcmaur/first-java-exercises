/**
La classe Rubrica permette di definire oggetti che rappresentano una
rubrica contenente piu' contatti (oggetti di tipo Contatto).
I contatti sono memorizzati in un array di oggetti Contatto.

NB: la rubrica potrebbe non essere completamente
riempita (potrebbe restare spazio per memorizzare contatti).
Pertanto, l'array puo' essere parzialmente riempito.
Si tenga conto di questo nello sviluppo dei metodi.

Piu' specificatamente, gli oggetti di tipo Rubrica contengono due variabili di istanza:
un array di oggetti Contatto e un contatore degli elementi gia` introdotti
nell'array.
Oltre ai metodi get() per accedere alle variabili private, si definiscano
i metodi inserisci(), cerca(), cancella(), modifica(), toString(), println(),
di gestione degli oggetti Rubrica,
completando opportunamente il codice riportato nel seguito.

NB: Rubrica.java NON HA MAIN: e' un generatore di oggetti "Rubrica"!!

*/


import java.util.Scanner;

public class Rubrica {
	private Contatto[] listacont; // array che mantiene i contatti inseriti in rubrica
    private int numcont;   // numero contatti presenti nella rubrica
	/** Costruttore di oggetti Rubrica.
	    Crea un oggetto Rubrica che puo' contenere al piu' n contatti
	    (riceve n come parametro).

		NB: Siccome la rubrica, appena creata, non contiene alcun contatto,
		il contatore che tiene traccia del numero di contatti
		inseriti deve essere inizializzato a 0.
	*/
    public Rubrica(int n) 
	{
		numcont=0;
		listacont=new Contatto [n];
		
    }

	// Restituisce il numero di contatti presenti in rubrica
    public int getnumcont() 
	{
      return numcont;
    }

    // Se c'e` ancora spazio nell'array dei contatti inserisce un nuovo contatto
    // e restituisce true, altrimenti restituisce false.
    public boolean inserisci(String nome,String cognome,int eta,boolean patente,String numeroDiTelefono) 
	{
		if(listacont.length==numcont)
			return false;
		else
		{
			listacont [numcont+1]=new Contatto (nome,cognome,eta,patente,numeroDiTelefono);
			numcont++;
			return true;
		}
	}

    

    // Visualizza a video tutti i contatti presenti in rubrica
    public void println()
	{
		for(int i=0;i<numcont;i++)
			System.out.println(listacont[i].toString());		
	}


    // Se la rubrica contiene almeno un contatto con il cognome specificato come parametro,
    // restituisce il primo contatto con tale cognome presente in rubrica;
    // altrimenti restituisce null.
    public Contatto cerca(String e) 
	{
      for(int i=0;i<numcont;i++)
	  {
		if(listacont[i].getCognome().compareTo(e)==0)
			return listacont[i];
	
	  }
		return null;
    }


    // Se la rubrica contiene almeno un contatto col cognome dato,
    // cancella il primo contatto con tale cognome e restituisce true, altrimenti restituisce false.
    public boolean cancella(String e) 
	{
		for(int i=0;i<numcont;i++)
	  {
		if(listacont[i].getCognome().compareTo(e)==0)
			listacont[i]=listacont[numcont-1];
			numcont--;
			return true;	
    	}
		return false;
	}



    //  Permette di modificare il primo Contatto con il cognome dato.
    //  Se tale Contatto esiste nella rubrica
    //  restituisce true, altrimenti false.
    //  Prende in input i nuovi dati da assegnare al contatto.
    public boolean modifica(String e) 
	{
		for(int i=0;i<numcont;i++)
	  {
		if(listacont[i].getCognome().compareTo(e)==0)
		{
			listacont[i].modificaContatto();
			return true;
		}
		else
			return false;
	  }
    }


// Restituisce sotto forma di stringa le informazioni di tutti i
// contatti memorizzati nella rubrica.
	public String toString()
	{
		String s="";
		for(int i=0;i<numcont;i++)
			s=listacont[i].toString();
		return s;
	}
}





