public class SimpleID3 {

    private String artist;
    private String album;
    private int year;

    public SimpleID3(String artist, String album, int year) {
        this.artist = artist;
        this.album = album;
        this.year = year;
    }

    /*
    ** Il seguente metodo restituisce -1 se this e` minore
    ** di id3 passato come parametro, 0 se uguale  e  1 se
    ** maggiore. this e` minore di id3 se l'anno  di  pub-
    ** blicazione di this e` minore, maggiore se l'anno e`
    ** maggiore. Se l'anno e` uguale allora this e` minore
    ** uguale o maggiore a seconda se il nome dell'artista
    ** e` minore, uguale o maggiore (lessicograficamente).
    */
    public int compareTo(SimpleID3 id3) {
		int n=0;
		if(this.getYear()!=id3.getYear())
		{
			if(this.getYear()<id3.getYear())
				n=-1;
			else
				n=1;
		}
		else
		{
			if(this.getArtist().equals(id3.getArtist()))
				n=0;
			else
			{
				if(this.getArtist().compareTo(id3.getArtist())==-1)
					n=-1;
				else
					n=1;
			}
		}
		return n;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

    public int getYear() {
        return year;
    }

    public String toString() {
        String temp = "";
        temp += "Artist: " + artist + "\n";
        temp += "Album:  " + album + "\n";
        temp += "Year:   " + year;
        return temp;
    }

}  
    

