public class SortUtil
{
	public static int [] BubbleSort (int [] s)
	{
		int z=0;
		for (int i=s.length-1;i>0;i--)
		{
			for (int j=0; j<i;j++)
			{
				if (s[j]>s[j+1])
				{
					z=s[j];
					s[j]=s[j+1];
					s[j+1]=z;
				}
			}
		}
		return s;
	}

	public static int [] InsertionSort (int [] dati)
	{
		int v=0,j=0;
		for (int i=1;i<dati.length;i++)
		{
			v=dati[i];
			j=i;
			while (j>0 && dati[j-1]>v)
			{
				dati [j]= dati [j-1];
				j--;
			}
			dati [j]=v;
		}
		return dati;
	}

	public static void SelectionSort(int[] x, int size) 
	{
		if(size==0)
			size=x.length;
	    for (int i=0; i<size-1; i++) 
		{
	        for (int j=i+1; j<size; j++) 
			{
	            if (x[i] > x[j]) 
				{
	                int temp = x[i];
	                x[i] = x[j];
	                x[j] = temp;
	            }
	        }
	    }
	}

	public static int [] mergeSort (int [] inpVett,int size)
	{
		int [] outVett= new int [inpVett.length];
		mergeSort0 ( inpVett, outVett, size);
	}

	private static void merge (int [] vett1,int [] vett2,int []outVett,int size1,int size2)
	{
		int i1 = 0;
		int i2 = 0;
		for (int iOut = 0; iOut < size1 + size2; iOut++)
		{
			if (i1 == size1)
			{
				outVett [iOut] = vett2 [i2];
				i2++;
			}
			else
			{
				if (i2 == size2)
				{
					outVett [iOut] = vett1 [i1];
					i1++;
				}
				else
				{
					if (vett1 [i1] > vett2 [i2])
					{
						outVett [iOut] = vett2 [i2];
						i2++;
					}
					else
					{
						outVett [iOut] = vett1 [i1];
						i1++;
					} 
				}
			}
		}
	}

	private static void mergeSort0 (int[] inpVett, int[] outVett, int size)
	{
		if (size == 1)
			outVett [0] = inpVett [0];
		else
		{
			int [] firstHalf;
			int [] secondHalf;
			int [] sortedFirstHalf;
			int [] sortedSecondHalf;
			int size1 = size / 2;
			int size2 = size – size1;
			firstHalf = new int [size1];
			sortedFirstHalf = new int [size1];
			secondHalf = new int [size2];
			sortedSecondHalf = new int [size2];
			for (int i = 0; i < size1; i++)
				firstHalf[i] = inpVett[i];
			for (int i = 0; i < size2; i++)
			secondHalf[i] = inpVett[size1 + i];
			mergeSort0 (firstHalf, sortedFirstHalf, size1);
			mergeSort0 (secondHalf, sortedSecondHalf, size2);
			merge (sortedFirstHalf, sortedSecondHalf, outVett, size1, size2);
		}
	}


	public static void main (String[]args)
	{
		int [] vett = ArrayUtil.read();
		//ArrayUtil.toString(SelectionSort(vett,0));
	}
}
