/*
Array di String parzialmente riempito e ordinato per ordinamento lessicografico

*/


public class StringArrayOrd {

   private int numElementi;
   private String[] elenco;


   public StringArrayOrd(int dim) {
      numElementi = 0;
	  elenco = new String[dim];

   }

/* se c'e' posto in elenco, inserisce una nuova stringa in ordine - elenco sempre ordinato */
   public boolean insOrd(String s) {
      if (numElementi<elenco.length) { // si puo' inserire perche' c'e' posto
      	int i=0;
      	while (i<numElementi && elenco[i].compareTo(s)<=0) {
								//elementi[] e' piu' piccolo di s
			i++;
			} // a questo punto i e' l'indice in cui inserire s
		for (int k=numElementi; k>i; k--) { //sposto a destra gli elementi per fare spazio
			elenco[k] = elenco[k-1];
		} // a questo punto posso inserire s in elenco[i]
		elenco[i] = s;
		numElementi++;
		return true;
	}
	else return false;
   }

   /* se la string passata come parametro e' presente in elenco, la cancella */
   public boolean cancella(String s)  {
	   int index = -1; // indice di elenco in cui si trova s
	   int i=0;
	   while (i<numElementi && // non ho ancora raggiunto la fine elenco
	          index==-1 && // non ho ancora trovato s in elenco
	          elenco[i].compareTo(s)<=0) { // non ho ancora trovato una stringa piu' grande
	   		if (s.equals(elenco[i])) // trovata s!
	   			index = i;
	   		i++;
		}
		if (index!=-1) { // ho trovato s in elenco
			for (int j=index; j<numElementi-1; j++) // elimino s compattando elenco
				elenco[j] = elenco[j+1];
			numElementi--;
			return true;
		}
		else return false;
	}

	public void print() {
		for (int i=0; i<numElementi; i++) {
			System.out.println(elenco[i]);
		}
	}
}



