public class Studente
{
	private static int numeroStud=0;
	private int matricola=0;
	private String nome="",cognome="";
	public Studente(String nome,String cognome)
	{
		this.nome = "";
		this.cognome = "";
		this.matricola=numeroStud+1;
		numeroStud ++;
	}

	public String getNome ()
	{
		return nome;
	}

	public String getCognome ()
	{
		return cognome;
	}

	public int getMatricola ()
	{
		return matricola;
	}

	public String toString()
	{
		return "Nome" + this.nome + "Cognome" + this.cognome + "Matricola" + this.matricola;
	}
}
