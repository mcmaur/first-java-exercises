import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class Subscribers extends JFrame implements ChangeListener
{
	private DefaultListModel subscribers;
	private JList list;
	private JRadioButton Mr,Ms,Mrs,Dr;
	private JTextField name;
	private JSlider eta;
	private JCheckBox GoldAccount;
	private JLabel labelSlider;
	
	public Subscribers()
	{
		super( "Subscribers" );

		//pannello titolo (radio buttons)-------------------------------------------------------

		JPanel titlePanel = new JPanel(  );
		JLabel titleDummy = new JLabel("Title:");
		titlePanel.add(titleDummy);
		
		Mr = new JRadioButton("Mr");
		Ms = new JRadioButton("Ms");
		Mrs = new JRadioButton("Mrs");
		Dr = new JRadioButton("Dr");
		ButtonGroup groupTitle = new ButtonGroup ();
		groupTitle.add(Mr);
		titlePanel.add(Mr);
		groupTitle.add(Ms);
		titlePanel.add(Ms);
		groupTitle.add(Mrs);
		titlePanel.add(Mrs);
		groupTitle.add(Dr);
		titlePanel.add(Dr);

		//pannello nome (label + textarea)-------------------------------------------------------

		JPanel namePanel = new JPanel();
		JLabel nameDummy = new JLabel("Name: ");
		namePanel.add(nameDummy);
		name = new JTextField ("Nome",15);
		namePanel.add(name);
		//[modificare]

		//pannello età (slider)-------------------------------------------------------

		JPanel agePanel = new JPanel();
		JLabel ageDummy = new JLabel("JSlider per eta\'");
		agePanel.add(ageDummy);
		eta = new JSlider(0, 100, 50);
		agePanel.add(eta);
		eta.addChangeListener(this);
		labelSlider = new JLabel("50");
		agePanel.add(labelSlider);
		//[modificare]



		//pannello gold account (check)-------------------------------------------------------

		JPanel goldPanel = new JPanel();
		GoldAccount = new JCheckBox ("Gold Account");
		goldPanel.add(GoldAccount);



		//pannello bottoni-------------------------------------------------------

		// create JButton for adding subscribers
		JButton addButton = new JButton( "Subscribe user" );
		addButton.addActionListener( new ActionListener() 
		{
			
			public void actionPerformed( ActionEvent event )
			{
				// prompt user for new name
				String user = "";
				if(Mr.isSelected())
					user="Mr";
				else if (Ms.isSelected())
					user="Ms";
				else if (Mrs.isSelected())
					user="Mrs";
				else if (Dr.isSelected())
					user="Dr";
				
				user = user+" "+name.getText();
				
				user = user+" , "+eta.getValue();
				
				if(GoldAccount.isSelected())
					user = user+" "+",gold account";
				//[modificare]

				// add new user to model
				subscribers.addElement( user );
			}
		});

		// create JButton for removing selected user
		JButton removeButton = new JButton( "Unsubscribe Selected user" );
		removeButton.addActionListener( new ActionListener() 
		{
			public void actionPerformed( ActionEvent event )
			{
				// remove selected user from model
				subscribers.removeElement(
				list.getSelectedValue() );
			}
		});

		// lay out GUI components
		JPanel inputPanel = new JPanel();
		inputPanel.add( addButton );
		inputPanel.add( removeButton );



		//pannello JList-------------------------------------------------------

		// create a DefaultListModel to store subscribers
		subscribers = new DefaultListModel();
		subscribers.addElement( "Mr Pink, 42" );
		subscribers.addElement( "Dr Searle, 55, gold account" );
		subscribers.addElement( "Dr Rubbia, 99" );
		subscribers.addElement( "Mrs Pacman, 80, gold account" );
		subscribers.addElement( "Mr Smith, 35" );
		subscribers.addElement( "Ms Smith, 32" );
		// create a JList for subscribers DefaultListModel
		list = new JList( subscribers );
		// allow user to select only one name at a time
		list.setSelectionMode(
		ListSelectionModel.SINGLE_SELECTION );
		JScrollPane listScrollPane = new JScrollPane(list);
		listScrollPane.setPreferredSize(new Dimension(200, 200));



		//main layout-------------------------------------------------------

		JPanel sub1 = new JPanel(); sub1.setLayout(new GridLayout(5,1));
		JPanel sub2 = new JPanel(); sub2.setLayout(new GridLayout(1,1));
		add(sub1, BorderLayout.CENTER); add(sub2, BorderLayout.SOUTH);

		sub1.add(titlePanel);
		sub1.add(namePanel);
		sub1.add(agePanel);
		sub1.add(goldPanel);
		sub1.add(inputPanel);
		sub2.add(listScrollPane);

		setDefaultCloseOperation( EXIT_ON_CLOSE );
		pack();
		setVisible( true );
	}// end Ex constructor

// execute application
public static void main( String args[] )
{
	new Subscribers();
}//end main

public void stateChanged(ChangeEvent e)
{
	JSlider js = (JSlider)e.getSource();
	labelSlider.setText(Integer.toString(js.getValue())); 
}

}//end public class subscribers
