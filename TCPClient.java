import java.io.*;
import java.net.*;

class TCPClient {
	
	public static void main(String argv[]) throws Exception{
		String sentence,request;
		String modifiedSentence;
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));		//Crea un flusso d'ingresso
		Socket clientSocket = new Socket("localhost", 6789);										//Crea unba socket client, connessa al socket
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());	//Crea un flusso di uscita connesso al socket
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream())); //Crea un flusso d'ingresso connesso al socket
		System.out.println("--Inserisci richiesta--\n");
		request = inFromUser.readLine();
		outToServer.writeBytes(request+ '\n');
		System.out.println("--Inserisci stringa--\n");
		sentence = inFromUser.readLine();
		outToServer.writeBytes(sentence + '\n');												//Invia una riga al server
		modifiedSentence = inFromServer.readLine();												//Legge la riga arrivata
		System.out.println("\nFROM SERVER: " + modifiedSentence);
		clientSocket.close();
	}
}

