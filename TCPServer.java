import java.io.*;
import java.net.*;

class TCPServer {
	
	public static void main(String argv[]) throws Exception
	{
		String clientSentence,clientRequest;
		String capitalizedSentence;
		ServerSocket welcomeSocket = new ServerSocket(6789);						//Crea una socket di benvenuto sulla porta 6789
		while(true)
		{
			Socket connectionSocket = welcomeSocket.accept();						//Attende su questa socket un contatto dal client
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));  //Crea un flusso d'ingresso collegato alla socket
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());  //Crea un flusso d'ingresso collegato alla socket
			clientRequest = inFromClient.readLine();								//Legge la riga dalla socket
// 			System.out.println("Richiesta: "+clientRequest);
			if(clientRequest.equals("1"))
			{
// 				System.out.println("Richiesta UpperCase");
				clientSentence = inFromClient.readLine();
				capitalizedSentence = clientSentence.toUpperCase() + '\n';
			}
			else
			{
// 				System.out.println("Richiesta LowerCase");
				clientSentence = inFromClient.readLine();
				capitalizedSentence = clientSentence.toLowerCase() + '\n';
			}
			outToClient.writeBytes(capitalizedSentence);							//Scrive la riga sulla socket
		}
	}
}
		

