public class TestContoCorrente {

   public static void main(String[] args) {

     //===TEST PRIMO COSTRUTTORE E METODI=========================

     ContoCorrente c1 = new ContoCorrente("Matteo", 100);

     // System.out.println(c1);
     if (!(c1.getNome().equals("Matteo")))
        System.out.println("ERRORE1.1 - ContoCorrente(String,double) oppure getNome()");

     if (!(c1.getSaldo()==100))
        System.out.println("ERRORE1.2 - ContoCorrente(String,double) oppure getSaldo()");

     if (!(c1.getIban().equals(ContoCorrente.IBAN_PREFIX+"0")))
        System.out.println("ERRORE1.3 - ContoCorrente(String,double) oppure getIban()");

     c1.versamento(-50);
     if (!(c1.getSaldo()==150))
        System.out.println("ERRORE1.4 - ContoCorrente(String,double) oppure versamento()");


     //===TEST (PROVVISORIO) SECONDO COSTRUTTORE=========================


     c1.setNome("Elio");

     if (!(c1.getNome().equals("Elio"))) System.out.println("ERRORE2.1 - setNome(String)");

     ContoCorrente c2 = new ContoCorrente("Ferruccio");

     System.out.println(c2);

 }

}