import java.util.*; 
public class TorreDiHanoi
{
	private int numDischi;

	public TorreDiHanoi(int num)
	{
		numDischi=num;
	}

	public void risolvi()
	{
		sposta(1,3,2,numDischi);
	}

	public void sposta (int da,int a, int altro,int n)
	{
		if(n==0)
			return;
		sposta(da,altro,a,n-1);
		spostaUnDisco(numDischi-n+1,da,a);
		sposta(altro,a,da,n-1);
	}

	public void spostaUnDisco(int disco,int da,int a)
	{
		System.out.println("Sposta disco "+disco+" dal piolo "+da+" al piolo "+a);
	}

	public static void main (String[]args)
	{
		Scanner tastiera=new Scanner (System.in);
		System.out.println("Quanti dischi?");
		int NumDischi=tastiera.nextInt();
		TorreDiHanoi Tor=new TorreDiHanoi(NumDischi);
		Tor.risolvi();
	}
}
