public class TriangoloRettangolo implements poligono 
{
	private double cat1,cat2;
	public TriangoloRettangolo(double c1,double c2)
	{
		cat1=c1;
		cat2=c2;
	}
	public double area()
	{
		return cat1*cat2/2;
	}
	public double perimetro()
	{
		return cat1+cat2+Math.sqrt(cat1*cat1+cat2*cat2);
	}
	public int numeroLati()
	{
		return 3;
	}
	public String toString()
	{
		return ("Sono un triangolo rettangolo. Le mie misure sono:\n area: "+area()+"\n perimetro: "+perimetro()+"\n num lati: "+numeroLati());
	}
}
