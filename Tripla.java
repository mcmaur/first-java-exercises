/** Gli oggetti di questa classe sono triple, composte da tre interi.
 */

public class Tripla {

  private int i;
  private int j;
  private int k;



  // NON INTRODURRE NESSUN COSTRUTTORE!
  // Si usa il costuttore di default.


  public int getI() {

   return i;
  }

  public int getJ() {

   return j;
  }

  public int getK() {

   return k;
  }

  public void setI(int i) {

   this.i = i;
  }

  public void setJ(int j) {

   this.j = j;
  }

  public void setK(int k) {

   this.k = k;
  }


  /**Da completare il metodo toString che metta sotto forma di stringa i valori dell'oggetto.*/
   public String toString(){}


}
