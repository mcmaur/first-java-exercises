/*Sviluppare la classe TrovaMin.java che:
- crea un array di interi chiedendo all'utente la dimensione dell'array
- inizializza l'array chiedendo all'utente i valori da tastiera
- visualizza a video (con ciclo FOR) il contenuto dell'array
- trova l'indice dell'elemento di valore minimo nell'array.
- visualizza a video l'indice del minimo ed il suo valore.
NB: usare un ciclo FOR per la visita dell'array.*/

import java.util.*;
public class TrovaMin
{
	public static void main (String [] v)
	{
		Scanner tastiera = new Scanner (System.in);
		int array [],min=Integer.MAX_VALUE,L=0,n=0;
		System.out.println("Inserisci numero componenti dell'array");
		n= tastiera.nextInt();
		array = new int [n];
		System.out.println("Inserisci i valori da immettere");
		for (int i=0;i<array.length;i++)
			array [i]=tastiera.nextInt();
		System.out.println("I valori immessi sono:");
		for (int i=0;i<array.length;i++)
			System.out.print(array [i]+" ");
		System.out.println();
		for (int k=0;k<array.length;k++)
			if (array[k]<min)
			{
				min=array[k];
				L=k;
			}
		System.out.println("Il valore minimo e': " + min+ " e si trova nella posizione " +L);


	}
}
