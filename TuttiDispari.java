/*Sviluppare la classe TuttiDispari.java che:
- crea ed inizializza un array di interi chiedendo all'utente la dimensione dell'array ed i valori degli elementi dell'array
- visualizza a video (con ciclo FOR) il contenuto dell'array
- verifica (con un ciclo WHILE) se l'array contiene solo numeri dispari.
- visualizza a video la scritta "sono tutti dispari" in caso positivo, "non sono tutti dispari" in caso negativo.
NB: se l'array contiene almeno un numero pari, il ciclo while deve fermarsi appena viene trovato tale numero. Altrimenti, analizza tutti gli elementi dell'array.*/

import java.util.*;
public class TuttiDispari
{
	public static void main (String[]args)
	{
	Scanner tastiera = new Scanner(System.in);
	int [] array;
	int n=0,k=0;
	boolean trovato=false;
	System.out.println("Inserisci numero componenti dell'array");
	n=tastiera.nextInt();
	array = new int [n];
	System.out.println("Inserisci i valori da immettere nell'array");
	for (int i=0;i<array.length;i++)
		array[i]=tastiera.nextInt();
	System.out.println("I valori immessi sono :");
	for (int i=0;i<array.length;i++)
		System.out.print(array[i]+" ");
	System.out.println();
	while (trovato==false && k<array.length)
		{
			if (array[k]%2==0)
				k++;
			else
			{
				System.out.println("Esiste almeno un valore dispari");
				trovato = true;
			}
		}	
	if (trovato==false)
		System.out.println("Non esiste alcun valore dispari");	
	}
}
