public class UsaListaConcatenata {

    public static void main(String[] args) {

        ListaConcatenata lista = new ListaConcatenata();

        LJV.Context showAllCtx = LJV.newContext( );
        showAllCtx.ignoreField("this$0");
        showAllCtx.outputFormat = "png";
        showAllCtx.ignorePrivateFields = false;

        lista.printList(); 
        lista.draw();
        System.out.println("");

        System.out.println("Inserisco 5:");
        lista.insert(5);
        lista.printList();
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();

        System.out.println("Inserisco 3:");
        lista.insert(3);
        lista.printList();        
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
 
        System.out.println("Inserisco 5:");
        lista.insert(5);
        lista.printList();
        lista.draw();
        System.out.println("");
        LJV.drawGraph(showAllCtx, lista );
        UtilLeggiTastiera.waitInput();

        System.out.println("2 e` nella lista? " + lista.member(2));
        System.out.println("5 e` nella lista? " + lista.member(5));
        System.out.println("3 e` nella lista? " + lista.member(5));
        System.out.println("");
        UtilLeggiTastiera.waitInput();

        System.out.println("Cancello 2: " + lista.delete_ter(2));
        lista.printList();
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
 
        System.out.println("Cancello 5: " + lista.delete_ter(5));
        lista.printList();
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
 
        System.out.println("Cancello 5: " + lista.delete_ter(5));
        lista.printList();
        lista.draw();
        System.out.println("");
        LJV.drawGraph(showAllCtx, lista );
        UtilLeggiTastiera.waitInput();
 
        System.out.println("Cancello 3: " + lista.delete_ter(3));
        lista.printList();
        lista.draw();
        System.out.println("");
        LJV.drawGraph(showAllCtx, lista );
        UtilLeggiTastiera.waitInput();

        System.out.println("Inserisco 7:");
        lista.insert(7);
        lista.printList();        
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
 
        System.out.println("Inserisco 1 in testa:");
        lista.insertFirst(1);
        lista.printList();
        System.out.println("Primo dato: " + lista.getFirstDato());
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();

        System.out.println("Inserisco 4 in coda:");
        lista.insertLast(4);
        lista.printList();
        System.out.println("Ultimo dato: " + lista.getLastDato());
        lista.draw();
        System.out.println("");
        LJV.drawGraph(showAllCtx, lista );
        UtilLeggiTastiera.waitInput();

        /*
        System.out.println("Inserisco 9 dopo il 7:");
        lista.insertAfter(9,7);
        lista.printList();
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
        */

        System.out.println("Cancello il primo:");
        System.out.println(lista.deleteFirst());
        lista.printList();
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
 
        System.out.println("Cancello l'ultimo:");
        lista.deleteLast();
        lista.printList();
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();

        System.out.println("Cancello l'ultimo:");
        lista.deleteLast();
        lista.printList();
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
 
    }

}
