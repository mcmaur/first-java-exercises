public class UsaReverse {

    public static void main(String[] args) {

        ListaConcatenata lista = new ListaConcatenata();

        LJV.Context showAllCtx = LJV.newContext( );
        showAllCtx.ignoreField("this$0");
        showAllCtx.outputFormat = "png";
        showAllCtx.ignorePrivateFields = false;

        lista.printList(); 
        lista.draw();
        System.out.println("");

        System.out.println("Inserisco 5:");
        lista.insert(5);
        lista.printList();
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();

        System.out.println("Inserisco 3:");
        lista.insert(3);
        lista.printList();        
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
 
        System.out.println("Inserisco 5:");
        lista.insert(5);
        lista.printList();
        lista.draw();
        System.out.println("");
        LJV.drawGraph(showAllCtx, lista );
        UtilLeggiTastiera.waitInput();

        System.out.println("Inserisco 7:");
        lista.insert(7);
        lista.printList();        
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
 
        System.out.println("Inserisco 1 in testa:");
        lista.insertFirst(1);
        lista.printList();
        System.out.println("Primo dato: " + lista.getFirstDato());
        lista.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();

        System.out.println("Inserisco 4 in coda:");
        lista.insertLast(4);
        lista.printList();
        System.out.println("Ultimo dato: " + lista.getLastDato());
        lista.draw();
        System.out.println("");
        LJV.drawGraph(showAllCtx, lista );
        UtilLeggiTastiera.waitInput();

        System.out.println("Faccio il reverse:");
        ListaConcatenata reverse = lista.reverseList();
        lista.printList();
        reverse.printList();
        lista.draw();
        reverse.draw();
        System.out.println("");
        UtilLeggiTastiera.waitInput();
 
    }

}
