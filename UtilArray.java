import java.util.Scanner;

class UtilArray {

    public static int[] leggiArrayDiInteriDaTastiera(int dimensione) {

        int[] arrayDiInteri = new int[dimensione];
        
        for (int i = 0; i < dimensione; i++)
            arrayDiInteri[i] = UtilLeggiTastiera.leggiInteroDaTastiera("Immetti l'elemento: " + i + ": ");

        return arrayDiInteri;
    }

    public static int[] leggiArrayDiInteriDaTastiera() {

        return leggiArrayDiInteriDaTastiera ( UtilLeggiTastiera.leggiInteroDaTastiera("Immetti dimensione dell'array: ") );

        /*
        int dimensione = UtilLeggiTastiera.leggiInteroDaTastiera("Immetti dimensione dell'array: ");

        int[] arrayDiInteri = leggiArrayDiInteriDaTastiera(dimensione);

        return arrayDiInteri;
        */    
 
    }

    public static void stampaArrayDiInteri(int[] arrayDiInteri) {
    
        System.out.println("L'array ha " + arrayDiInteri.length + " elementi, questi sono:");

        for (int i=0; i < arrayDiInteri.length; i++)
            System.out.println("Elemento " + i + ": " + arrayDiInteri[i]);

    }

    public static int[] leggiArrayDiInteriDaTastieraTipoPIN(int dimensione) {

        int[] arrayDiInteri = new int[dimensione];
        
        for (int i = 0; i < dimensione; i++)
            arrayDiInteri[i] = UtilLeggiTastiera.leggiInteroDaTastieraTipoPIN("Immetti l'elemento " + i + ": ");

        return arrayDiInteri;
    }

    public static String[] leggiArrayDiStringheDaTastiera(int dimensione) {

        String[] arrayDiStringhe = new String[dimensione];

        for (int i = 0; i < dimensione; i++)
            arrayDiStringhe[i] = UtilLeggiTastiera.leggiStringaDaTastiera("Inserisci la stringa " + i + ": ");
 
        return arrayDiStringhe;
    }

    public static String[] leggiArrayDiStringheDaTastiera() {

        int dimensione = UtilLeggiTastiera.leggiInteroPositivoDaTastiera("Inserisci la dimensione dell'array di stringhe: ");
      
        return leggiArrayDiStringheDaTastiera(dimensione);
    }

    public static void main(String[] args) {
   
        int[] a = leggiArrayDiInteriDaTastiera();
        stampaArrayDiInteri(a);


        /*

        // FOR CLASSICO
        for (int i=0; i < a.length; i++)
            System.out.println("Elemento " + i + ": " + a[i]);

        // FOR EACH
        for (int x: a)
            System.out.println("Elemento: " + x);

        int[] b = leggiArrayDiInteriDaTastieraTipoPIN(5);

        // FOR CLASSICO
        for (int i=0; i < 5; i++)
            System.out.println("Elemento " + i + ": " + b[i]);

        // FOR EACH
        for (int x: b)
            System.out.println("Elemento: " + x);
        */

    }

}
        
