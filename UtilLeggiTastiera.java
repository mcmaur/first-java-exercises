import java.util.Scanner;

class UtilLeggiTastiera {

    static final int MAX_TENTATIVI = 3;

    public static int leggiInteroDaTastieraTipoPIN(String messaggio) {

        Scanner tastiera = new Scanner(System.in);
        int tentativo = 1;    

        System.out.print(messaggio);
        while( (!tastiera.hasNextInt()) && (tentativo < MAX_TENTATIVI) ) {
            tentativo++;
            tastiera.next();
            System.out.print("Immetti un intero, per favore (hai ancora " + (MAX_TENTATIVI - tentativo + 1) + " tentativi): ");
        }
        
        if (tentativo > MAX_TENTATIVI)
            //System.out.println("Hai superato il numero di tentativi"); 
            throw new RuntimeException("Hai superato il numero di tentativi");
        else
            return tastiera.nextInt();
    }

    public static int leggiInteroDaTastiera(String messaggio) {

        Scanner tastiera = new Scanner(System.in);
    
        System.out.print(messaggio);
        while(!tastiera.hasNextInt()) {
            tastiera.next();
            System.out.print("Immetti un intero, per favore: ");
        }

        return tastiera.nextInt();

    }

    public static int leggiInteroPositivoDaTastiera(String messaggio) {

        int numero = leggiInteroDaTastiera(messaggio);
        while (!(numero >= 0)) {
            System.out.println("Immetti un intero positivo: ");
            numero = leggiInteroDaTastiera(messaggio);
        }
        return numero;

    }

    public static String leggiStringaDaTastiera(String messaggio) {

        Scanner tastiera = new Scanner(System.in);
    
        System.out.print(messaggio);

        /*
        String temp = tastiera.nextLine();

        if (temp == null) System.out.println("stinga nulla");
        if (temp.equals("")) System.out.println("stringa vuota");
        return temp;
        */

        return tastiera.nextLine();
        
    }

    public static void waitInput() {
        Scanner tastiera = new Scanner(System.in);
        tastiera.nextLine();
    }

    public static void main(String[] args) {
   
        // TEST METODI DI QUESTA CLASSE
     
        String test = leggiStringaDaTastiera("Inserisci una stringa: ");
        System.out.println("Stringa inserita: ->" + test + "<-");

        /*
        int k = leggiInteroPositivoDaTastiera("Dimmi un intero positivo: ");
        System.out.println("Numero intero postivo immesso: " + k);

        int j = leggiInteroPositivoDaTastiera("Dimmi un intero positivo: ");
        System.out.println("Numero intero postivo immesso: " + j);

        int i = leggiInteroDaTastieraTipoPIN("Dimmi un intero: ");
        System.out.println("Numero intero immesso: " + i);
        */

    }

}
        
