import java.io.File;
import java.util.Scanner;


public class CercaFile {
	
	public static void main (String args[]) {
		Scanner tast = new Scanner(System.in);
		System.out.println("Immetti il nome del file da cercare");		
		String nomeFile=tast.nextLine();
		System.out.println("Immetti il nome del direttorio in cui cercare");		
		String nomeDir=tast.nextLine();
		File [] listaFile = currentDirectory.listFiles();
		while(i<listaFile.length)
		{
			if (cercaFile(new File (nomeDir),nomeFile)!=null)
			{
				File file=cercaFile(new File (nomeDir),nomeFile);
				System.out.println("file trovato");
				System.out.println(file.getPath());
			}
			else
				System.out.println("file non trovato");
			i++;
		}
	}

	private static File cercaFile(File cartella,String nome){
		File [] files = cartella.listFiles();
		if (files==null) return null;
		for (File file:files)
		{
			if(file.isDirectory()){
				File trov=cercaFile(file,nome);
				if (trov!=null) return trov;
			}
			else if (file.getName().equalsIgnoreCase(nome)) return file;
		}
		return null;
	}
}

