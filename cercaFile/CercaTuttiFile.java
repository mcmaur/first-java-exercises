import java.io.*;
import java.util.*;
import java.lang.*;
public class CercaTuttiFile
{
	private static int contaNumFile=0;

	public static void cercaTuttiFile(File currentDirectory,String fileDaCercare,String CartellaDestinazione)
	{
		int i=0;
		File FileTrovato=null;
		File [] listaFile = currentDirectory.listFiles();
		try
		{
			while(i<listaFile.length)
			{
				if(listaFile[i].isFile())
				{
					if(fileDaCercare.charAt(0)!='*')
					{		
						if(listaFile[i].getName().compareToIgnoreCase(fileDaCercare)==0)
						{
							contaNumFile=contaNumFile+1;
							System.out.println(contaNumFile);
							System.out.println("Trovato:"+ listaFile[i].getPath());
							System.out.println("Inserisco in: " + CartellaDestinazione+listaFile[i].getName()+"["+ contaNumFile +"]");
							copia(listaFile[i].getPath(),CartellaDestinazione+listaFile[i].getName()+"["+ contaNumFile +"]");
						}
					}
					else
					{
						boolean stillEgual=true;
						int contFileDir=listaFile[i].getName().length()-1;
						int contFileDaCercare=fileDaCercare.length()-1;
						while(stillEgual && contFileDaCercare>0)
						{
							//System.out.println("Confronto "+listaFile[i].getName().charAt(contFileDir)+" di "+ listaFile[i].getName()+" con "+ fileDaCercare.charAt(contFileDaCercare));
							if(listaFile[i].getName().charAt(contFileDir)!=fileDaCercare.charAt(contFileDaCercare))
							{
								stillEgual=false;
							}
							contFileDaCercare--;
							contFileDir--;
						}
						if(stillEgual)
						{
							contaNumFile++;
							System.out.println("Trovato:"+ listaFile[i].getPath());
							copia(listaFile[i].getPath(),CartellaDestinazione+listaFile[i].getName()+"["+ contaNumFile +"]");
						}
					}
				}	
				else
				{
					if(listaFile[i].isDirectory())
					{
						cercaTuttiFile(listaFile[i],fileDaCercare,CartellaDestinazione);
					}
					
				}
				i++;
			}
		}
		catch(Exception Ex)
		{
			System.out.println("Errore");
		}
	}
	
	public static void copia(String origine, String destinazione) throws Exception
	{
		try
		{
			FileInputStream fis = new FileInputStream(origine);
			FileOutputStream fos = new FileOutputStream(destinazione);
			byte [] dati = new byte[fis.available()];
			fis.read(dati);
			fos.write(dati);
			fis.close();
			fos.close();
		}
		catch (FileNotFoundException FNFE)
		{
			System.out.println("File non trovato");
		}
		catch (SecurityException SE)
		{
			System.out.println("Permessi negati");
		}
		catch (IOException IOE)
		{
			System.out.println("errore di I/O");
		}
	}

	public static void main (String[]args)
	{
		Scanner tastiera = new Scanner(System.in);
		System.out.println("Immetti il nome del file da cercare");		
		String fileDaCercare=tastiera.nextLine();
		//System.out.println("Immetti il nome del direttorio in cui cercare");		
		String directoryDiPartenza="/home/mauro";//tastiera.nextLine();
		//System.out.println("Immetti percorso destinazione");
		String CartellaDestinazione=args[0];//tastiera.nextLine();
		//System.out.println(CartellaDestinazione);
		cercaTuttiFile (new File(directoryDiPartenza),fileDaCercare,CartellaDestinazione);
	}
}
