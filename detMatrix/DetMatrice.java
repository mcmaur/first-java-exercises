import java.util.*;
public class DetMatrice
{
	public static void main (String[]args)
	{
		boolean notContinue=true;
		Scanner tastiera=new Scanner(System.in);
		System.out.println("Mauro Cerbai grazie all'aiuto prezioso di Paolo Inaudi è orgoglioso di presentare il CALCOLATORE DI DETERMINANTI :)");
		System.out.println("Version 1.0.1");
		double [][] matrice=Input(tastiera);
		print(matrice,matrice[0].length);
		while(notContinue==true)
		{
			notContinue=false;
			System.out.println("Risulta essere la matrice che volevi scrivere? si o no");
			String modify=tastiera.next();
			if(modify.equals("no"))
			{
				modificaElemento(matrice,tastiera,matrice[0].length);
			}
			else
			{
				if(modify.equals("si"))
				{
					notContinue=false;			
				}
				else
				{
					System.out.println("Risposta non contemplata");
					notContinue=true;	
				}
			}
		}
		System.out.println("Procedo con il calcolo del determinante");
		System.out.println(calcoloDeterminante(matrice));
	}

	private static double [][] Input(Scanner tastiera)
	{
		System.out.println("Dimensione?");
		int dim=tastiera.nextInt();
		double [][] matrice=new double [dim][dim];
		System.out.println("Leggimi la matrice da in alto a sinistra ad in basso a destra");
		int y=0,x=0;
		inserimentoUtente(y,x,matrice,tastiera);
		return matrice;
	}

	private static void inserimentoUtente(int y,int x,double [][] matrice,Scanner tastiera)
	{
		int yplus=y+1;
		int xplus=x+1;
		int dim=matrice[0].length;
		if (x<dim)
		{
			System.out.println("Posizione ("+yplus+","+xplus+"):");
			while(!tastiera.hasNextDouble())
			{
				tastiera.next();
				System.out.println("Inseriro un numero prego");
			}
			matrice [y][x]=tastiera.nextDouble();
			inserimentoUtente(y,xplus,matrice,tastiera);
		}
		else
		{
			if(y<(dim-1))
			{
				x=0;
				inserimentoUtente(yplus,x,matrice,tastiera);
			}
			else
			{
				return;
			}
		}
	}

	private static void print(double [][] matrice,int dim)
	{
		int y=0;
		System.out.print("    ");
		for (double number=1;number<dim+1;number++)
		{
			System.out.print(number+"  ");
		}
		System.out.println();
		System.out.print("   |----------------------------------------------------------------");
		System.out.println();
		int numberFor=0;
		while(numberFor<dim)
		{
			int yplus=y+1;
			System.out.print(yplus+".0|");
			for(int x=0;x<dim;x++)
			{
				System.out.print(matrice[y][x]+ "  ");
			}
			numberFor++;
			y++;
			System.out.println();
		}
		System.out.println();
	}

	private static void modificaElemento(double [][]matrice,Scanner tastiera,int dim)
	{
		boolean ready=false;
		do
		{
			int x=0,y=0;
			double n=0;
			System.out.println("Dimmi la posizione che vuoi cambiare: prima la componente verticale (y) e poi l'orizzontale (x)");
			System.out.println();
			do
			{
			System.out.println("Specifica la riga:");
			while(!tastiera.hasNextInt())
			{
				tastiera.next();
				System.out.println("Immetti almeno un intero xD");
			}
			y=tastiera.nextInt();
			}
			while(y<1 || y> dim);
			do
			{
			System.out.println("Specifica la colonna");
			while(!tastiera.hasNextInt())
			{
				tastiera.next();
				System.out.println("Immetti almeno un intero xD");
			}
			x=tastiera.nextInt();
			}
			while(x<1 || x> dim);
			System.out.println("Dimmi il valore che vuoi inserire");
			while(!tastiera.hasNextDouble())
			{
				tastiera.next();
				System.out.println("Immetti almeno un intero xD");
			}
			n=tastiera.nextDouble();
			matrice [(y-1)][(x-1)]=n;
			print(matrice,matrice[0].length);
			System.out.println("Vuoi cambiare ancora? si o no");
			String modify=tastiera.next();
			if(modify.equals("si"))
				ready=true;
			else
				ready=false;
		}
		while(ready==true);
	}

	private static double calcoloDeterminante(double [][] matrice)
	{
		int j=matrice[0].length-1;
		double Det=0;
		if(matrice[0].length==1)
			return matrice[0][0];
		else
		{
			for(int i=0;i<j;i++)
				Det=Det+matrice[i][j]*Math.pow(-1.0,(j+i))*calcoloDeterminante(matriceTagliata(matrice,i));
		}
		return Det;
	}

	private static double [][] matriceTagliata (double [][]array,int xRemove)
	{
		int dim2=array[0].length-1,x=0,y=0;
		double [][] arr2=new double [dim2][dim2];
		for (int i=0;i<dim2;i++)
		{
			for(int j=0;j<dim2;j++)
			{
				if(i<xRemove)
				{
					arr2[i][j]=array[i][j];
				}
				else
				{
					arr2[i][j]=array[i+1][j];
				}
			}
		}
		
		return arr2;
	}

}
