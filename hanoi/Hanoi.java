public class Hanoi
{
	enum Piolo {SINISTRO, CENTRALE, DESTRO}

	static void hanoi(int n, Piolo orig, Piolo aus, Piolo dest) 
	{
		if(n == 1) spostaIlDisco(orig, dest);
		else 
		{
			hanoi(n-1, orig, dest, aus);
			spostaIlDisco(orig, dest);
			hanoi(n-1, aus, orig, dest);
		}
	}

	public static void spostaIlDisco(Piolo orig, Piolo dest)
	{
		System.out.println("Sposta il disco da "+orig+" a "+dest);
	}

	public static void main(String[] args) 
	{
		hanoi(Integer.parseInt(args[0]), Piolo.SINISTRO, Piolo.CENTRALE, Piolo.DESTRO);
	}
}
