import java.util.*;
public class Sudoku
{
	public static void main (String[]args)
	{
		Scanner tastiera= new Scanner (System.in);
		boolean flagOriz=false,flagVerti=false;
		int scelta=0,n=0,sum=0;
		int [][]sudo =new int [9][9];
		System.out.println();
		System.out.println("The ideator: mcmaur\n"
		+"Un ringraziamento a Paolo Inaudi il codice di verifica\n"
		+"PRESENT\n"
		+"THE M&C-SUDOKU\n"
		+"Version: 1.1.0");
		System.out.println();
		insert1(sudo);
		visual(sudo);
		while(scelta!=5)
		{
			SistOut();
			while(!tastiera.hasNextInt())
			{
				tastiera.next();
				System.out.println("Immetti almeno un intero xD");
			}
			scelta=tastiera.nextInt();
			if (scelta==1)
			{
				InserimentoUtente(sudo);
				insert1(sudo);
				visual(sudo);
			}
			else
				if (scelta==2)
				{
					flagOriz=verificaOrizz(sudo);
					flagVerti=verificaVerti(sudo);
					if(flagOriz && flagVerti)
						System.out.println("OK");	
					else
						System.out.println("ERROR");
				}
		}
	}

		//suggerita da paolo inaudi
		public static boolean verificaOrizz(int [][] sudo)
		{
			boolean [] messi=new boolean [9];
			for (int r=0;r<9;++r)
			{//reinizializzo ad ogni riga
				for (int i = 0; i < 9; i++) 
				{
					messi[i]=false;
				}
				for (int c = 0; c < 9; c++) 
				{
					messi[sudo[r][c]]=true;
				}
				for (int i = 0; i < 9; i++) 
				{
				if(!messi[i]) 
					return false;
				}
			}
			return true;
		}

		public static boolean verificaVerti(int [][] sudo)
		{
			boolean [] messi=new boolean [9];
			for (int c=0;c<9;++c)
			{//reinizializzo ad ogni riga
				for (int i = 0; i < 9; i++) 
				{
					messi[i]=false;
				}
				for (int r = 0; r < 9; r++) 
				{
					messi[sudo[r][c]]=true;
				}
				for (int i = 0; i < 9; i++) 
				{
				if(!messi[i]) 
					return false;
				}
			}
			return true;
		}

		//vecchie verifiche
/*		public static boolean verificaMoltiOriz(int [][] sudo)
		{
			boolean flag=true;
			int molti=1;
			while(flag==true)
			{
				for (int j=0;j<9;j++)
				{
					for (int i=0;i<9;i++)
						molti=molti*sudo[j][i];
					if(molti!=362880)
						flag=false;
				}
			}
			return flag;
		}

		public static boolean verificaMoltiVerti(int [][] sudo)
		{
			boolean flag=true;
			int molti=1;
			while(flag==true)
			{
				for (int j=0;j<9;j++)
				{
					for (int i=0;i<9;i++)
						molti=molti*sudo[i][j];
					if(molti!=362880)
						flag=false;
				}
			}
			return flag;
		}

		public static boolean verificaAddOriz ( int [][] sudo )
		{
			boolean flag=true;
			int sum=0;
			while(flag==true)
			{
				for (int j=0;j<9;j++)
				{
					for (int i=0;i<9;i++)
						sum=sum+sudo[j][i];
					if(sum!=45)
						flag=false;
				}
			}
			return flag;
		}

		public static boolean verificaAddVerti ( int [][] sudo )
		{
			boolean flag=true;
			int sum=0;
			while(flag==true)
			{
				for (int j=0;j<9;j++)
				{
					for (int i=0;i<9;i++)
						sum=sum+sudo[i][j];
					if(sum!=45)
						flag=false;
				}
			}
			return flag;
		}*/

		public static void insert1 (int [][] sudo)
		{
			sudo [0][4]=4;sudo [1][1]=9;sudo [1][2]=3;sudo [1][6]=2;sudo[1][7]=8;sudo [2][1]=2;sudo [2][3]=3;sudo [2][5]=7;sudo [2][7]=4;sudo [3][2]=7;sudo [3][6]=9;sudo [4][0]=3;sudo [4][4]=5;sudo [4][8]=1;sudo [5][2]=5;sudo [5][6]=7;sudo [6][1]=7;sudo [6][3]=5;sudo [6][3]=5;sudo [6][5]=8;sudo [6][7]=9;sudo [7][1]=3;sudo [7][2]=9;sudo [7][6]=8;sudo [7][7]=6;sudo [8][4]=1;
		}

		public static void SistOut()
		{
			System.out.println("Quale azione vuoi svolgere?");
			System.out.println("1:inserire elemento");
			System.out.println("2:verifica del sudoku completo");
			System.out.println("5:uscita dall'applicazione");
		}

		public static void InserimentoUtente(int [][] sudo)
		{
			Scanner tastiera=new Scanner (System.in);
			int x=0,y=0,n=0;
			System.out.println("Dimmi la posizione in cui vuoi inserirlo prima la componente verticale (y) e poi l'orizzontale (x)");
			System.out.println();
			do
			{
			System.out.println("Specifica la riga:");
			while(!tastiera.hasNextInt())
			{
				tastiera.next();
				System.out.println("Immetti almeno un intero xD");
			}
			y=tastiera.nextInt();
			}
			while(y<1 || y>9);
			do
			{
			System.out.println("Specifica la colonna");
			while(!tastiera.hasNextInt())
			{
				tastiera.next();
				System.out.println("Immetti almeno un intero xD");
			}
			x=tastiera.nextInt();
			}
			while(x<1 || x>9);
			do
			{
			System.out.println("Dimmi il valore che vuoi inserire");
			while(!tastiera.hasNextInt())
			{
				tastiera.next();
				System.out.println("Immetti almeno un intero xD");
			}
			n=tastiera.nextInt();
			}
			while(n>9 || n<0);
			sudo [(y-1)][(x-1)]=n;
		}

		public static void visual ( int [][] sudo)
		{
			System.out.print("  ");
			for (int i =1;i<10;i++)
			{
				if (i==4 || i==7)
					System.out.print("  ");
				System.out.print(i+" ");
			}
			System.out.println();
			System.out.print(" |---------------------");
			System.out.println();
			System.out.print(1 + "|");
			for (int i =0;i<9;i++)
			{
				if (i==3 || i==6)
					System.out.print("| ");
				System.out.print(sudo[0][i]+ " ");
			}
			System.out.println();
			System.out.print(2 + "|");
			for (int i =0;i<9;i++)
			{
				if (i==3|| i==6)
					System.out.print("| ");
				System.out.print(sudo[1][i]+ " ");
			}
			System.out.println();
			System.out.print(3 + "|");
			for (int i =0;i<9;i++)
			{
				if (i==3 || i==6)
					System.out.print("| ");
				System.out.print(sudo[2][i]+ " ");
			}
			System.out.println();
			System.out.print(" |------+-------+------");
			System.out.println();
			System.out.print(4 + "|");
			for (int i =0;i<9;i++)
			{
				if (i==3 || i==6)
					System.out.print("| ");
				System.out.print(sudo[3][i]+" ");
			}
			System.out.println();
			System.out.print(5 + "|");
			for (int i =0;i<9;i++)
			{
				if (i==3 || i==6)
					System.out.print("| ");
				System.out.print(sudo[4][i]+" ");
			}
			System.out.println();
			System.out.print(6 + "|");
			for (int i =0;i<9;i++)
			{
				if (i==3 || i==6)
					System.out.print("| ");
				System.out.print(sudo[5][i]+" ");
			}
			System.out.println();
			System.out.print(" |------+-------+------");
			System.out.println();
			System.out.print(7 + "|");
			for (int i =0;i<9;i++)
			{
				if (i==3 || i==6)
					System.out.print("| ");
				System.out.print(sudo[6][i]+" ");
			}
			System.out.println();
			System.out.print(8 + "|");
			for (int i =0;i<9;i++)
			{
				if (i==3 || i==6)
					System.out.print("| ");
				System.out.print(sudo[7][i]+" ");
			}
			System.out.println();
			System.out.print(9 + "|");
			for (int i =0;i<9;i++)
			{
				if (i==3 || i==6)
					System.out.print("| ");
				System.out.print(sudo[8][i]+" ");
			}
			System.out.println();
			System.out.println();
		}
}
